import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'app/services/login.service';
import { UtilService } from 'app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-verify-account',
  templateUrl: './verify-account.component.html',
  styleUrls: ['./verify-account.component.scss']
})
export class VerifyAccountComponent implements OnInit {

  emailUser: string;
  codeVerify: number;
  path: string;

  constructor(private utilService: UtilService, private router: Router, private activateRoute: ActivatedRoute, private loginService: LoginService) { }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((objt: any) => {
      console.log(objt);
      this.emailUser = objt.email;
      this.path = objt.path;
    });
  }

  back() {
    this.router.navigate(['/login']);
  }

  openDialog(): void {
    Swal.fire({
      title: '¿Que hacer si tengo problemas con la plataforma?',
      html: 'Contamos con un equipo de soporte para ayudarte a responder tus FAQ, <a href="https://www.google.com/">escríbenos</a> o puedes comunicarte via email al correo: soportefoam@foam.com.',
      confirmButtonColor: '#ef8354',
      confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> Entendido!',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }

  verifyAccount() {
    const formverify = {
      email: this.emailUser,
      codeVerify: this.codeVerify
    }
    console.log(formverify);
    this.loginService.verifyCode(formverify)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        if (this.path === 'login') {
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Usuario Activo',
            text: 'Ya puedes iniciar sesión con tu correo',
            showConfirmButton: false,
            timer: 2500
          });
          this.router.navigateByUrl('/login');
        } else {
          const obj = {
            email: this.emailUser
          }
          this.utilService.goToChangePassword(obj);
        }
      }
    })
  }

}
