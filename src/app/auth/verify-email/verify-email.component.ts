import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'app/services/login.service';
import { UtilService } from 'app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {

  email: string;
  loader: boolean = false;

  constructor(private router: Router, private utilService: UtilService, private loginService: LoginService) { }

  ngOnInit(): void {
  }

  back() {
    this.router.navigate(['/login']);
  }

  openDialog(): void {
    Swal.fire({
      title: '¿Que hacer si tengo problemas con la plataforma?',
      html: 'Contamos con un equipo de soporte para ayudarte a responder tus FAQ, <a href="https://www.google.com/">escríbenos</a> o puedes comunicarte via email al correo: soportefoam@foam.com.',
      confirmButtonColor: '#ef8354',
      confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> Entendido!',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }

  verifyEmail() {
    window.scrollTo(0, 0);
    this.loader = true;
    const formverify = {
      email: this.email,
      path: 'reset'
    }
    this.loginService.verifyEmail(formverify)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        this.loader = true;
        this.utilService.goToVerifyAccount(formverify);
      }
    })
  }

}
