import { Component, inject, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'app/services/login.service';
import { UtilService } from 'app/services/util.service';
import Swal from 'sweetalert2';

declare const google: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit{

  @ViewChild('googleBtn') googleBtn: ElementRef;

  hide: boolean;
  formSubmitted: boolean = false;
  displayPosition: boolean = false;
  position: string = '';
  documentsTypes: any[];
  loader: boolean = false;
  emailUser: any;

  private fb = inject(FormBuilder);
  private router = inject(Router);
  private loginService = inject(LoginService);
  private utilService = inject(UtilService);

  constructor() {

  }

  public loginForm = this.fb.group({
    email: ['' ,Validators.required],
    password: ['',Validators.required],
  });

  public registryForm = this.fb.group({
    first_name: ['', Validators.required],
    firs_last_name: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    isActive: false,
    termsAcceptance: [false]
  });

  ngOnInit(): void {
    this.hide = true;
    this.documentsTypes = [
      {value: 'Cedula de ciudadanía', viewValue: 'Cédula de ciudadania'},
      {value: 'Cedula de extranjería', viewValue: 'Cédula de extranjeria'},
    ]
  }

  ngAfterViewInit(): void {
    this.googleInit();
  }

  googleInit() {
    google.accounts.id.initialize({
      client_id: "301157551379-sikd704ffb35194a196e3s277qas8pck.apps.googleusercontent.com",
      callback: (response: any) => this.handleCredentialResponse(response)
    });
    google.accounts.id.renderButton(
      this.googleBtn.nativeElement,
      { theme: "outline", size: "extra_large", text: "Iniciar sesión con google",  }  // customization attributes
    );
  }

  handleCredentialResponse(response: any) {
    console.log("Encoded JWT ID token: " + response.credential);
    this.loginService.loginGoogle(response.credential).subscribe(resp => {
      this.loader = true;
      Swal.fire({
        position: 'top',
        icon: 'success',
        title: 'Sesión iniciada',
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigateByUrl('/dashboard');
    });
  }

  openDialog(): void {
    Swal.fire({
      title: '¿Que hacer si tengo problemas con la plataforma?',
      html: 'Contamos con un equipo de soporte para ayudarte a responder tus FAQ, <a href="https://www.google.com/">escríbenos</a> o puedes comunicarte via email al correo: soportefoam@foam.com.',
      confirmButtonColor: '#ef8354',
      confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> Entendido!',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }

  invalidInput(campo: string): boolean {
    if (this.loginForm.get(campo)?.invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  login() {
    this.formSubmitted = true;
    this.loginService.login(this.loginForm.value)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        this.loader = true;
        this.router.navigateByUrl('/dashboard');
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Sesión iniciada',
          showConfirmButton: false,
          timer: 1500
        });
      }
    })
  }

  forgotPassword() {
    this.router.navigate(['/verify-email']);
  }

  registry() {
    this.formSubmitted = true;
    window.scrollTo(0, 0);
    this.loader = true;
    if(!this.registryForm.value.termsAcceptance) {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Términos y condiciones',
        text: 'Debes aceptar nuestros términos y condiciones para registarte exitosamente.',
        showConfirmButton: false,
        timer: 2500
      });
    } else if(this.registryForm.value.password.length < 8){
      Swal.fire({
        position: 'center',
        icon: 'error',
        text: 'La contraseña debe tener al menos 8 caracteres.',
        showConfirmButton: false,
        timer: 2500
      });
    } else {
      this.emailUser = this.registryForm.value.email;
      this.loginService.registry(this.registryForm.value)
      .subscribe( {
        error: (e:any) => {
          Swal.fire('Error', e.error.error.message, 'error');
        },
        next: (res) => {
          this.registryForm.reset();
          this.loader = false;
          console.log('value email')
          console.log(this.emailUser)
          const obj = {
            email: this.emailUser,
            path: 'login'
          }
          this.utilService.goToVerifyAccount(obj);
        }
      })
    }
  }

}
