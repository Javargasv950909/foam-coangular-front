import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'app/services/login.service';
import { UtilService } from 'app/services/util.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit {

  password: string;
  passwordtwo: string;
  loader: boolean = false;
  emailUser: string;

  constructor(private activateRoute: ActivatedRoute ,private router: Router, private utilService: UtilService, private loginService: LoginService) { }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((objt: any) => {
      console.log(objt);
      this.emailUser = objt.email;
    });
  }

  back() {
    this.router.navigate(['/login']);
  }

  changePassword() {
    const formverify = {
      email: this.emailUser
    }
    if(this.password === this.passwordtwo) {
      if(this.password.length > 8) {
        this.loginService.updatePassword(formverify, this.password)
        .subscribe( {
          error: (e:any) => {
            Swal.fire('Error', e.error.error.message, 'error');
          },
          next: (res) => {
            this.loader = true;
            Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Contraseña Actualizada',
              text: 'Ya puedes iniciar sesión con tu nueva contraseña',
              showConfirmButton: false,
              timer: 2500
            });
            this.router.navigateByUrl('/login');
          }
        })
      } else {
        Swal.fire({
          position: 'top',
          icon: 'error',
          title: 'Contraseña Inválida',
          text: 'La contraseña debe tener al menos 8 caracteres.',
          showConfirmButton: false,
          timer: 2500
        });
      }
    } else {
      Swal.fire({
        position: 'top',
        icon: 'error',
        title: 'Contraseñas diferentes',
        text: 'Comprueba las contraseñas. Deben ser exactamente iguales',
        showConfirmButton: false,
        timer: 2500
      });
    }
  }

}
