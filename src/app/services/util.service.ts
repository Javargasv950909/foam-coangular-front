import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CONSTANTS_URIS } from 'app/constants/constants.uris';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private router: Router) { }


    /**
   * Metodo gérico para guardar valores en el sessionStorage
   */
     saveInSessionStorage(key: string, value: any) {
      sessionStorage.setItem(key, value);
    }
  
    /**
     * Metodo gérico para guardar valores en el localStorage
     */
     saveInLocalStorage(key: string, value: any) {
      localStorage.setItem(key, value);
    }
  
    /**
     * Metodo gérico para obtejer un valor del localStorage
     */
  
    getLocalStorage(key: string): any {
      return localStorage.getItem(key);
    }
  
    /**
     * Metodo gérico para obtejer un valor del sessionStorage
     */
  
      getSessionStorage(key: string): any {
      return sessionStorage.getItem(key);
    }
  
    /**
     * Metodo gérico para obtejer eliminar cache del localStorage
     */
  
    clearLocalStorage() {
      localStorage.clear();
    }

    /**
     * Metodo gérico para dirigir al detalle del inmueble con los queryparams
     */

    goToFichInmueble(inmueble: any) {
      this.router.navigate([CONSTANTS_URIS.View_inmueble], {queryParams: inmueble})
    }

    /**
     * Metodo gérico para dirigir al detalle del vehiculo con los queryparams
     */

    goToFichVehiculo(vehiculo: any) {
      this.router.navigate([CONSTANTS_URIS.View_vehiculo], {queryParams: vehiculo})
    }

    /**
     * Metodo gérico para dirigir al detalle de la sociedad con los queryparams
     */

    goToFichSociedad(sociedad: any) {
      this.router.navigate([CONSTANTS_URIS.View_sociedad], {queryParams: sociedad})
    }

    /**
     * Metodo gérico para dirigir al detalle del vehiculo con los queryparams
     */

    goToFichPoliza(poliza: any) {
      this.router.navigate([CONSTANTS_URIS.View_poliza], {queryParams: poliza})
    }

    /**
     * Metodo gérico para dirigir al detalle de inversion con los queryparams
     */

    goToFichInversion(inversion: any) {
      this.router.navigate([CONSTANTS_URIS.View_inversion], {queryParams: inversion})
    }

    /**
     * Metodo gérico para dirigir al detalle de p bancario con los queryparams
     */

    goToFichPbancario(pbancario: any) {
      this.router.navigate([CONSTANTS_URIS.View_pbancario], {queryParams: pbancario})
    }

    /**
     * Metodo gérico para dirigir al detalle de prestamo con los queryparams
     */

    goToFichPrestamo(prestamo: any) {
      this.router.navigate([CONSTANTS_URIS.View_prestamo], {queryParams: prestamo})
    }

    /**
     * Metodo gérico para dirigir al detalle de salario con los queryparams
     */

     goToFichSalario(salario: any) {
      this.router.navigate([CONSTANTS_URIS.View_salario], {queryParams: salario})
    }

    /**
     * Metodo gérico para dirigir al detalle de salario con los queryparams
     */

    goToFichMembresia(membresia: any) {
      this.router.navigate([CONSTANTS_URIS.View_membresia], {queryParams: membresia})
    }

    /**
     * Metodo gérico para dirigir al detalle de salario con los queryparams
     */

    goToVerifyAccount(email: any) {
      this.router.navigate([CONSTANTS_URIS.Verify], {queryParams: email})
    }

    /**
     * Metodo gérico para dirigir al cambio de contraseña con queryparams
     */

    goToChangePassword(email: any) {
      this.router.navigate([CONSTANTS_URIS.New_password], {queryParams: email})
    }
}
