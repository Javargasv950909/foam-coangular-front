import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'app/interfaces/user.interface';
import { environment } from 'environments/environment';
import { LoginService } from './login.service';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class ActivoService {

  user: User;
  constructor(private http: HttpClient, private loginService: LoginService) {
    this.user = this.loginService.getCurrentUser();
  }

  getImueble(id: string) {
    return this.http.get(`${base_url}/activos/getInmueble/${id}`);
  }

  getVehiculo(id: string) {
    return this.http.get(`${base_url}/activos/getVehiculo/${id}`);
  }

  getSociedad(id: string) {
    return this.http.get(`${base_url}/activos/getSociedad/${id}`);
  }

  getPoliza(id: string) {
    return this.http.get(`${base_url}/activos/getPoliza/${id}`);
  }

  getInversion(id: string) {
    return this.http.get(`${base_url}/activos/getInversion/${id}`);
  }

  getPbancario(id: string) {
    return this.http.get(`${base_url}/activos/getProductobancario/${id}`);
  }

  getPrestamo(id: string) {
    return this.http.get(`${base_url}/activos/getPrestamo/${id}`);
  }

  getSalario(id: string) {
    return this.http.get(`${base_url}/activos/getSalario/${id}`);
  }

  getMembresia(id: string) {
    return this.http.get(`${base_url}/activos/getMembresia/${id}`);
  }

  getDocumento(id: string) {
    return this.http.get(`${base_url}/documents/getDocument/${id}`);
  }

  updateInmueble(idInmueble: string, inmuebleData: any) {
    return this.http.put(`${base_url}/activos/updateInmueble/${idInmueble}`, inmuebleData);
  }

  updateVehiculo(idVehiculo: string, vehiculoData: any) {
    return this.http.put(`${base_url}/activos/updateVehiculo/${idVehiculo}`, vehiculoData);
  }

  updateSociedad(idSociedad: string, sociedadData: any) {
    return this.http.put(`${base_url}/activos/updateSociedad/${idSociedad}`, sociedadData);
  }

  updatePoliza(idPoliza: string, polizaData: any) {
    return this.http.put(`${base_url}/activos/updatePoliza/${idPoliza}`, polizaData);
  }

  updateInversion(idInversion: string, inversionData: any) {
    return this.http.put(`${base_url}/activos/updateInversion/${idInversion}`, inversionData);
  }

  updatePbancario(idPbancario: string, productoData: any) {
    return this.http.put(`${base_url}/activos/updateProbancario/${idPbancario}`, productoData);
  }

  updatePrestamo(idPrestamo: string, prestamoData: any) {
    return this.http.put(`${base_url}/activos/updatePrestamo/${idPrestamo}`, prestamoData);
  }

  updateSalario(idSalario: string, salarioData: any) {
    return this.http.put(`${base_url}/activos/updateSalario/${idSalario}`, salarioData);
  }

  updateMembresia(idMembresia: string, membresiaData: any) {
    return this.http.put(`${base_url}/activos/updateMembresia/${idMembresia}`, membresiaData);
  }

  deleteActivo(idActivo: string, idcatActivo: string) {
    return this.http.delete(`${base_url}/activos/deleteActivo/${idActivo}/${idcatActivo}`)
  }

  async uploadFileInmueble(idInmueble: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadInmueble/${idInmueble}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFileVehiculo(idVehiculo: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadVehiculo/${idVehiculo}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFileSociedad(idSociedad: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadSociedad/${idSociedad}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFilePoliza(idPoliza: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadPoliza/${idPoliza}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFileInversion(idInversion: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadInversion/${idInversion}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFileProductoB(idPbancario: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadPbancario/${idPbancario}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFilePrestamo(idPrestamo: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadPrestamo/${idPrestamo}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFileSalario(idSalario: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadSalario/${idSalario}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async uploadFileMembresia(idMembresia: string, archivo: File, nameCustomFile: string) {
    try {

      const url = `${base_url}/documents/uploadMembresia/${idMembresia}/${nameCustomFile}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const resp = await fetch(url,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  newReminderforActive(idActivo: any, id_subtg: string, formDataReminder: any) {
    return this.http.post(`${base_url}/reminders/newReminder/${idActivo}/${id_subtg}/${this.user._id}`,formDataReminder);
  }

  newReminderforUser(formDataReminder: any) {
    return this.http.post(`${base_url}/reminders/newReminderUser/${this.user._id}`,formDataReminder);
  }

  deleteReminder(idReminder: any) {
    return this.http.delete(`${base_url}/reminders/deleteReminder/${idReminder}`);
  }

}
