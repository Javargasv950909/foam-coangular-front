import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map, tap } from 'rxjs/operators';
import { UtilService } from './util.service';
import { environment } from '../../environments/environment';
import { Observable, of } from 'rxjs';
import { User } from 'app/interfaces/user.interface';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  currentUser: User;

  constructor(private http: HttpClient, private router:Router, private utilService: UtilService) { }


  verificarToken():Observable<boolean>{

    const token = this.utilService.getLocalStorage('token') || '';

    return this.http.get(`${base_url}/auth/check-token`,{
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }).pipe(
      tap((resp:any)=>{
        this.currentUser = resp.user;
        this.utilService.saveInLocalStorage('token',resp.token);
      }),
      map(resp => true),
      catchError(error => of(false))
    )
  }


  login(formData: any) {
    return this.http.post(`${base_url}/auth/login`,formData)
          .pipe(
            tap((resp: any) => {
              this.currentUser = resp.user;
              console.log('current user');
              console.log(this.currentUser);
              this.utilService.saveInLocalStorage('token',resp.token);
             })
           )
  }

  loginGoogle(token: any) {
    return this.http.post(`${base_url}/auth/login/google/`, { token })
          .pipe(
            tap((resp: any) => {
              this.currentUser = resp.user;
              console.log('current user');
              console.log(this.currentUser);
              this.utilService.saveInLocalStorage('token',resp.token);
             })
           )
  }

  registry(formDataRegistry: any) {
    return this.http.post(`${base_url}/user/newUser`,formDataRegistry);
  }

  verifyCode(formDataVerify: any) {
    return this.http.post(`${base_url}/auth/login/verify-account`,formDataVerify);
  }

  verifyEmail(formDataVerify: any) {
    return this.http.post(`${base_url}/user/verify-account`,formDataVerify);
  }

  updatePassword(formDataVerify: any, password: any) {
    return this.http.post(`${base_url}/user/update-password/${password}`,formDataVerify);
  }

  logOut() {
    this.utilService.clearLocalStorage();
    this.router.navigateByUrl('/login');
  }

  getCurrentUser() {
    return this.currentUser;
  }
}
