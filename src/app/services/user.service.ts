import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'app/interfaces/user.interface';
import { environment } from 'environments/environment';
import { LoginService } from './login.service';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: User;
  url: string;
  constructor(private http: HttpClient, private loginService: LoginService) { 
    this.user = this.loginService.getCurrentUser();
  }

  getUser() {
    return this.http.get(`${base_url}/user/user/${this.user._id}`);
  }

  newActivo(categoria: any, formDataActivo: any) {
    switch(categoria) {
      case 'Bienes inmuebles':
        return this.http.post(`${base_url}/activos/newInmueble/${this.user._id}`,formDataActivo);
      case 'Vehiculos':
        return this.http.post(`${base_url}/activos/newVehiculo/${this.user._id}`,formDataActivo);
      case 'Sociedades':
        return this.http.post(`${base_url}/activos/newSociedad/${this.user._id}`,formDataActivo);
      case 'Polizas de seguro':
        return this.http.post(`${base_url}/activos/newPoliza/${this.user._id}`,formDataActivo);
      case 'Inversiones':
        return this.http.post(`${base_url}/activos/newInversion/${this.user._id}`,formDataActivo);
      case 'Productos bancarios':
        return this.http.post(`${base_url}/activos/newPbancario/${this.user._id}`,formDataActivo);
      case 'Prestamos':
        return this.http.post(`${base_url}/activos/newPrestamo/${this.user._id}`,formDataActivo);
      case 'Salarios':
        return this.http.post(`${base_url}/activos/newSalario/${this.user._id}`,formDataActivo);
      case 'Membresias/Suscripciones':
        return this.http.post(`${base_url}/activos/newMembresia/${this.user._id}`,formDataActivo);
    }
  }

  updateUser(idUser: string, userData: any) {
    console.log('userData')
    console.log(userData)
    return this.http.put(`${base_url}/user/updateUser/${idUser}`, userData);
  }

  async uploadFileUser(idUser: string, archivo: File, nameCustomFile: string, idSub: string, categoria: string, subcategoria: string, num_documento: string, notas: string) {
    try {
      this.url = `${base_url}/documents/uploadUser/${idUser}/${nameCustomFile}/${idSub}`;

      const formData = new FormData();

      formData.append('file', archivo);

      const queryParams = new URLSearchParams({
        categoria,
        subcategoria,
        documento: num_documento,
        notas,
      });
      
      const resp = await fetch(`${this.url}?${queryParams}`,{
        method: 'POST',
        body: formData
      })

      const data = await resp.json();

      if(data.ok) {
        return data.imagen
      } else {
        console.log(data.msg)
        return false;
      }
      
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async deleteFileUser(idFile) {
    return this.http.delete(`${base_url}/documents/deleteDocument/${idFile}`);
  }
}
