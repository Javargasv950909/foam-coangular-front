import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { WhatHaveComponent } from 'app/what-have/what-have.component';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ViewInmuebleComponent } from 'app/view-inmueble/view-inmueble.component';
import { ViewVehiculoComponent } from 'app/view-vehiculo/view-vehiculo.component';
import { ViewSociedadComponent } from 'app/view-sociedad/view-sociedad.component';
import { ViewInversionComponent } from 'app/view-inversion/view-inversion.component';
import { ViewPbancarioComponent } from 'app/view-pbancario/view-pbancario.component';
import { ViewPrestamosComponent } from 'app/view-prestamos/view-prestamos.component';
import { ViewSalarioComponent } from 'app/view-salario/view-salario.component';
import { ViewMembresiaComponent } from 'app/view-membresia/view-membresia.component';
import { ViewPolizaComponent } from 'app/view-poliza/view-poliza.component';
import { DocumentsUserComponent } from 'app/documents-user/documents-user.component';
import { CalendarComponent } from 'app/calendar/calendar.component';

// Material imports
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NewActiveComponent } from 'app/new-active/new-active.component';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { LoaderComponent } from 'app/components/loader/loader.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ManualComponent } from 'app/manual/manual.component';

// full calendar 

import { FullCalendarModule } from '@fullcalendar/angular';
import { FaqComponent } from 'app/faq/faq.component';
import { SoporteComponent } from 'app/soporte/soporte.component';
import { CashformatPipe } from 'app/pipes/cashformat.pipe';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatSelectModule,
    MatTabsModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    FullCalendarModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    WhatHaveComponent,
    NewActiveComponent,
    ViewInmuebleComponent,
    LoaderComponent,
    ModalCustomComponent,
    ViewVehiculoComponent,
    ViewSociedadComponent,
    ViewInversionComponent,
    ViewPbancarioComponent,
    ViewPrestamosComponent,
    ViewSalarioComponent,
    ViewMembresiaComponent,
    ViewPolizaComponent,
    DocumentsUserComponent,
    CalendarComponent,
    FaqComponent,
    SoporteComponent,
    ManualComponent
  ],
  providers: [
    CashformatPipe
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})

export class AdminLayoutModule {}
