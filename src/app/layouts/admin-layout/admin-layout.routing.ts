import { Routes } from '@angular/router';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { WhatHaveComponent } from 'app/what-have/what-have.component';
import { NewActiveComponent } from 'app/new-active/new-active.component';
import { ViewInmuebleComponent } from 'app/view-inmueble/view-inmueble.component';
import { CONSTANTS_URIS } from 'app/constants/constants.uris';
import { ViewVehiculoComponent } from 'app/view-vehiculo/view-vehiculo.component';
import { ViewSociedadComponent } from 'app/view-sociedad/view-sociedad.component';
import { ViewInversionComponent } from 'app/view-inversion/view-inversion.component';
import { ViewPbancarioComponent } from 'app/view-pbancario/view-pbancario.component';
import { ViewPrestamosComponent } from 'app/view-prestamos/view-prestamos.component';
import { ViewSalarioComponent } from 'app/view-salario/view-salario.component';
import { ViewMembresiaComponent } from 'app/view-membresia/view-membresia.component';
import { ViewPolizaComponent } from 'app/view-poliza/view-poliza.component';
import { DocumentsUserComponent } from 'app/documents-user/documents-user.component';
import { CalendarComponent } from 'app/calendar/calendar.component';
import { FaqComponent } from 'app/faq/faq.component';
import { SoporteComponent } from 'app/soporte/soporte.component';
import { ManualComponent } from 'app/manual/manual.component';
import { IsAuthenticatedGuard } from 'app/guards/is-authenticated.guard';

export const AdminLayoutRoutes: Routes = [
    { path: CONSTANTS_URIS.Dashboard,      component: DashboardComponent, canActivate: [ IsAuthenticatedGuard ]},
    { path: CONSTANTS_URIS.What_have,      component: WhatHaveComponent},
    { path: CONSTANTS_URIS.New_activo,   component: NewActiveComponent },
    { path: CONSTANTS_URIS.View_inmueble,     component: ViewInmuebleComponent },
    { path: CONSTANTS_URIS.View_vehiculo,     component: ViewVehiculoComponent },
    { path: CONSTANTS_URIS.View_sociedad,     component: ViewSociedadComponent },
    { path: CONSTANTS_URIS.View_inversion,     component: ViewInversionComponent },
    { path: CONSTANTS_URIS.View_pbancario,     component: ViewPbancarioComponent },
    { path: CONSTANTS_URIS.View_prestamo,     component: ViewPrestamosComponent },
    { path: CONSTANTS_URIS.View_salario,     component: ViewSalarioComponent },
    { path: CONSTANTS_URIS.View_membresia,     component: ViewMembresiaComponent },
    { path: CONSTANTS_URIS.View_poliza,     component: ViewPolizaComponent },
    { path: CONSTANTS_URIS.Documents_user,     component: DocumentsUserComponent },
    { path: CONSTANTS_URIS.User_profile,     component: UserProfileComponent },
    { path: CONSTANTS_URIS.Calendar,     component: CalendarComponent },
    { path: CONSTANTS_URIS.Faqs,     component: FaqComponent },
    { path: CONSTANTS_URIS.Support,     component: SoporteComponent },
    { path: CONSTANTS_URIS.Manual,     component: ManualComponent }
];
