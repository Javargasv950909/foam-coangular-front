import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSociedadComponent } from './view-sociedad.component';

describe('ViewSociedadComponent', () => {
  let component: ViewSociedadComponent;
  let fixture: ComponentFixture<ViewSociedadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewSociedadComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewSociedadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
