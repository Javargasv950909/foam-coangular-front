import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Sociedad } from 'app/interfaces/sociedades.interface';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ActivoService } from 'app/services/activo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-sociedad',
  templateUrl: './view-sociedad.component.html',
  styleUrls: ['./view-sociedad.component.scss']
})
export class ViewSociedadComponent implements OnInit {

  @ViewChild(MatPaginator) paginatorImpuestos: MatPaginator;
  @ViewChild(MatSort) sortImpuestos: MatSort;
  @ViewChild(MatPaginator) paginatorRenovaciones: MatPaginator;
  @ViewChild(MatSort) sortRenovaciones: MatSort;
  @ViewChild(MatPaginator) paginatorObligaciones: MatPaginator;
  @ViewChild(MatSort) sortObligaciones: MatSort;
  @ViewChild(MatPaginator) paginatorContactos: MatPaginator;
  @ViewChild(MatSort) sortContactos: MatSort;
  @ViewChild(MatPaginator) paginatorDocumentos: MatPaginator;
  @ViewChild(MatSort) sortDocumentos: MatSort;
  @ViewChild(MatPaginator) paginatorEventos: MatPaginator;
  @ViewChild(MatSort) sortEventos: MatSort;
  @ViewChild(MatPaginator) paginatorTodos: MatPaginator;
  @ViewChild(MatSort) sortTodos: MatSort;

  loader: boolean;
  sociedad: Sociedad;
  impuestos: MatTableDataSource<any>;
  renovaciones: MatTableDataSource<any>;
  obligaciones: MatTableDataSource<any>;
  contactos: MatTableDataSource<any>;
  documentos: MatTableDataSource<any>;
  eventos: MatTableDataSource<any>;
  todos: MatTableDataSource<any>;
  ultimosImpuestos: any[] = [];
  ultimosRenovaciones: any[] = [];
  ultimosObligaciones: any[] = [];
  ultimosContactos: any[] = [];
  ultimosEventos: any[] = [];
  ultimosTodos: any[] = [];
  displayedColumnsImpuestos: string[] = ['Nombre impuesto', 'Fecha de pago', 'Periocidad', 'Valor', 'Tipo moneda'];
  displayedColumnsRenovaciones: string[] = ['Nombre', 'Fecha de pago', 'Periocidad', 'valor', 'Moneda'];
  displayedColumnObligaciones: string[] = ['Nombre', 'Fecha de pago', 'Periocidad', 'valor', 'Moneda'];
  displayedColumnsContactos: string[] = ['Nombre', 'Empresa', 'Telefono', 'Celular', 'Correo'];
  displayedColumnsDocumentos: string[] = ['Nombre del documento', 'Acciones'];
  displayedColumnsEventos: string[] = ['Fecha', 'Nota'];
  displayedColumnsTodos: string[] = ['State', 'Nombre', 'Fecha'];
  edicion: boolean;
  documento: File;
  nombreDocumento: string;
  colorBoton = '#bdbdbd';

  constructor(private activateRoute: ActivatedRoute, private activoService: ActivoService, public dialog: MatDialog) {
    this.loader = false;
    this.edicion = false;
   }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((params: any) => {
      this.loader = true;
      this.infoSociedad(params._id);
    });
  }

  infoSociedad(id: any) {
    this.activoService.getSociedad(id)
      .subscribe((sociedad: Sociedad) => {
        this.sociedad = sociedad;
        this.impuestos = new MatTableDataSource<any>(sociedad.impuestos);
        this.ultimosImpuestos = this.sociedad.impuestos;
        this.eventos = new MatTableDataSource<any>(sociedad.eventos);
        this.ultimosEventos = this.sociedad.eventos;
        this.todos = new MatTableDataSource<any>(sociedad.todos);
        this.ultimosTodos = this.sociedad.todos;
        this.todos.filterPredicate = (data: any) => !data.state;
        this.todos.filter = 'excludeFalse';
        this.renovaciones = new MatTableDataSource<any>(sociedad.renovaciones_administrativas_documentarias);
        this.ultimosRenovaciones = this.sociedad.renovaciones_administrativas_documentarias;
        this.obligaciones = new MatTableDataSource<any>(sociedad.obligaciones_laborales);
        this.ultimosObligaciones = this.sociedad.obligaciones_laborales;
        this.contactos = new MatTableDataSource<any>(sociedad.contactos);
        this.ultimosContactos = this.sociedad.contactos;
        this.documentos = new MatTableDataSource<any>(sociedad.documentos);
        this.loader = false;
      })
  }

  ngAfterViewInit() {
    this.impuestos.paginator = this.paginatorImpuestos;
    this.impuestos.sort = this.sortImpuestos;
    this.contactos.paginator = this.paginatorContactos;
    this.contactos.sort = this.sortContactos;
    this.documentos.paginator = this.paginatorDocumentos;
    this.documentos.sort = this.sortDocumentos;
    this.renovaciones.paginator = this.paginatorRenovaciones;
    this.renovaciones.sort = this.sortRenovaciones;
    this.obligaciones.paginator = this.paginatorObligaciones;
    this.obligaciones.sort = this.sortObligaciones;
    this.eventos.paginator = this.paginatorEventos;
    this.eventos.sort = this.sortEventos;
    this.todos.paginator = this.paginatorTodos;
    this.todos.sort = this.sortTodos;
  }

  applyFilterImpuestos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.impuestos.filter = filterValue.trim().toLowerCase();

    if (this.impuestos.paginator) {
      this.impuestos.paginator.firstPage();
    }
  }

  applyFilterRenovaciones(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.renovaciones.filter = filterValue.trim().toLowerCase();

    if (this.renovaciones.paginator) {
      this.renovaciones.paginator.firstPage();
    }
  }

  applyFilterObligaciones(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.obligaciones.filter = filterValue.trim().toLowerCase();

    if (this.obligaciones.paginator) {
      this.obligaciones.paginator.firstPage();
    }
  }

  applyFilterContactos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contactos.filter = filterValue.trim().toLowerCase();

    if (this.contactos.paginator) {
      this.contactos.paginator.firstPage();
    }
  }

  applyFilterDocumentos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  applyFilterEventos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.eventos.filter = filterValue.trim().toLowerCase();

    if (this.eventos.paginator) {
      this.eventos.paginator.firstPage();
    }
  }

  applyFilterTodos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.todos.filter = filterValue.trim().toLowerCase();

    if (this.todos.paginator) {
      this.todos.paginator.firstPage();
    }
  }

  editFicha() {
    this.edicion = true;
    this.colorBoton = '#ef8354';
  }

  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'impuestosSociedad':
            this.ultimosImpuestos.push(result);
            this.impuestos = new MatTableDataSource<any>(this.ultimosImpuestos);
            break;
          case 'renovacionesSociedad':
            this.ultimosRenovaciones.push(result);
            this.renovaciones = new MatTableDataSource<any>(this.ultimosRenovaciones);
            break;
          case 'obligacionesSociedad':
            this.ultimosObligaciones.push(result);
            this.obligaciones = new MatTableDataSource<any>(this.ultimosObligaciones);
            break;
          case 'contactos':
            this.ultimosContactos.push(result);
            this.contactos = new MatTableDataSource<any>(this.ultimosContactos);
            break;
          case 'eventos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosEventos);
            this.ultimosEventos.push(result);
            this.eventos = new MatTableDataSource<any>(this.ultimosEventos);
            break;
          case 'todos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosTodos);
            this.ultimosTodos.push(result);
            this.todos = new MatTableDataSource<any>(this.ultimosTodos);
            break;
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            Swal.fire({
              title: 'Cargue de documentos',
              text: "Al cargar el documento se recargará la ventana y perderás la información editada en los anteriores ítems. Se recomienda primero guardar los datos anteriores. ¿Deseas continuar?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ef8354',
              cancelButtonColor: '#bdbdbd',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si'
            }).then((result) => {
              if (result.isConfirmed) {
                this.loader = true;
                this.activoService.uploadFileSociedad(this.sociedad._id, this.documento, this.nombreDocumento)
                .then(res => {
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Documento cargado exitosamente',
                    showConfirmButton: false,
                    timer: 2500
                  });
                  window.location.reload();
                })
                .catch((error) => {
                  Swal.fire('Error', error.error.error.message, 'error');
                });
              } else if (result.isDenied) {
                return;
              }
            })
            break;
        }
      } 
    });
  }

  guardar() {
    this.sociedad.impuestos = this.ultimosImpuestos;
    this.sociedad.obligaciones_laborales = this.ultimosObligaciones;
    this.sociedad.renovaciones_administrativas_documentarias = this.ultimosRenovaciones;
    this.sociedad.contactos = this.ultimosContactos;
    this.sociedad.eventos = this.ultimosEventos;
    this.activoService.updateSociedad(this.sociedad._id, this.sociedad)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Sociedad Actualizada Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
      }
    })
  }

}
