import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { PolizaSeguro } from 'app/interfaces/poliza_seguro.interface';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ActivoService } from 'app/services/activo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-poliza',
  templateUrl: './view-poliza.component.html',
  styleUrls: ['./view-poliza.component.scss']
})
export class ViewPolizaComponent implements OnInit {

  @ViewChild(MatPaginator) paginatorPrimas: MatPaginator;
  @ViewChild(MatSort) sortPrimas: MatSort;
  @ViewChild(MatPaginator) paginatorContactos: MatPaginator;
  @ViewChild(MatSort) sortContactos: MatSort;
  @ViewChild(MatPaginator) paginatorDocumentos: MatPaginator;
  @ViewChild(MatSort) sortDocumentos: MatSort;
  @ViewChild(MatPaginator) paginatorEventos: MatPaginator;
  @ViewChild(MatSort) sortEventos: MatSort;
  @ViewChild(MatPaginator) paginatorTodos: MatPaginator;
  @ViewChild(MatSort) sortTodos: MatSort;

  loader: boolean;
  poliza: PolizaSeguro;
  primas: MatTableDataSource<any>;
  contactos: MatTableDataSource<any>;
  documentos: MatTableDataSource<any>;
  ultimasPrimas: any[] = [];
  ultimosContactos: any[] = [];
  ultimosEventos: any[] = [];
  ultimosTodos: any[] = [];
  eventos: MatTableDataSource<any>;
  todos: MatTableDataSource<any>;
  displayedColumnPrimas: string[] = ['Nombre', 'Fecha de pago', 'Periocidad', 'valor', 'Moneda'];
  displayedColumnsContactos: string[] = ['Nombre', 'Empresa', 'Telefono', 'Celular', 'Correo'];
  displayedColumnsDocumentos: string[] = ['Nombre del documento', 'Acciones'];
  displayedColumnsEventos: string[] = ['Fecha', 'Nota'];
  displayedColumnsTodos: string[] = ['State', 'Nombre', 'Fecha'];
  edicion: boolean;
  documento: File;
  nombreDocumento: string;
  colorBoton = '#bdbdbd';

  constructor(private activateRoute: ActivatedRoute, private activoService: ActivoService, public dialog: MatDialog) {
    this.loader = false;
    this.edicion = false;
  }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((params: any) => {
      this.loader = true;
      this.infoPoliza(params._id);
    });
  }

  infoPoliza(id: any) {
    this.activoService.getPoliza(id)
      .subscribe((poliza: PolizaSeguro) => {
        this.poliza = poliza;
        this.primas = new MatTableDataSource<any>(poliza.pago_prima);
        this.ultimasPrimas = this.poliza.pago_prima;
        this.contactos = new MatTableDataSource<any>(poliza.contactos);
        this.ultimosContactos = this.poliza.contactos;
        this.documentos = new MatTableDataSource<any>(poliza.documentos);
        this.eventos = new MatTableDataSource<any>(poliza.eventos);
        this.ultimosEventos = this.poliza.eventos;
        this.todos = new MatTableDataSource<any>(poliza.todos);
        this.todos.filterPredicate = (data: any) => !data.state;
        this.todos.filter = 'excludeFalse';
        this.ultimosTodos = this.poliza.todos;
        this.loader = false;
      })
  }

  ngAfterViewInit() {
    this.primas.paginator = this.paginatorPrimas;
    this.primas.sort = this.sortPrimas;
    this.contactos.paginator = this.paginatorContactos;
    this.contactos.sort = this.sortContactos;
    this.documentos.paginator = this.paginatorDocumentos;
    this.documentos.sort = this.sortDocumentos;
    this.eventos.paginator = this.paginatorEventos;
    this.eventos.sort = this.sortEventos;
    this.todos.paginator = this.paginatorTodos;
    this.todos.sort = this.sortTodos;
  }

  applyFilterPrimas(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.primas.filter = filterValue.trim().toLowerCase();

    if (this.primas.paginator) {
      this.primas.paginator.firstPage();
    }
  }

  applyFilterContactos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contactos.filter = filterValue.trim().toLowerCase();

    if (this.contactos.paginator) {
      this.contactos.paginator.firstPage();
    }
  }

  applyFilterDocumentos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  applyFilterEventos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.eventos.filter = filterValue.trim().toLowerCase();

    if (this.eventos.paginator) {
      this.eventos.paginator.firstPage();
    }
  }

  editFicha() {
    this.edicion = true;
    this.colorBoton = '#ef8354';
  }

  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'pagosPrimas':
            this.ultimasPrimas.push(result);
            this.primas = new MatTableDataSource<any>(this.ultimasPrimas);
            break;
          case 'contactos':
            this.ultimosContactos.push(result);
            this.contactos = new MatTableDataSource<any>(this.ultimosContactos);
            break;
          case 'eventos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosEventos);
            this.ultimosEventos.push(result);
            this.eventos = new MatTableDataSource<any>(this.ultimosEventos);
            break;
          case 'todos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosTodos);
            this.ultimosTodos.push(result);
            this.todos = new MatTableDataSource<any>(this.ultimosTodos);
            break;
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            Swal.fire({
              title: 'Cargue de documentos',
              text: "Al cargar el documento se recargará la ventana y perderás la información editada en los anteriores ítems. Se recomienda primero guardar los datos anteriores. ¿Deseas continuar?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ef8354',
              cancelButtonColor: '#bdbdbd',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si'
            }).then((result) => {
              if (result.isConfirmed) {
                this.loader = true;
                this.activoService.uploadFilePoliza(this.poliza._id, this.documento, this.nombreDocumento)
                .then(res => {
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Documento cargado exitosamente',
                    showConfirmButton: false,
                    timer: 2500
                  });
                  window.location.reload();
                })
                .catch((error) => {
                  Swal.fire('Error', error.error.error.message, 'error');
                });
              } else if (result.isDenied) {
                return;
              }
            })
            break;
        }
      } 
    });
  }

  guardar() {
    this.poliza.pago_prima = this.ultimasPrimas;
    this.poliza.contactos = this.ultimosContactos;
    this.poliza.eventos = this.ultimosEventos;
    this.activoService.updatePoliza(this.poliza._id, this.poliza)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Poliza Actualizada Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
      }
    })
  }


}
