import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPolizaComponent } from './view-poliza.component';

describe('ViewPolizaComponent', () => {
  let component: ViewPolizaComponent;
  let fixture: ComponentFixture<ViewPolizaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewPolizaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
