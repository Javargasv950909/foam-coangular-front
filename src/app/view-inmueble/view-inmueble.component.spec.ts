import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInmuebleComponent } from './view-inmueble.component';

describe('ViewInmuebleComponent', () => {
  let component: ViewInmuebleComponent;
  let fixture: ComponentFixture<ViewInmuebleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewInmuebleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewInmuebleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
