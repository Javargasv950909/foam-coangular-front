import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Inmueble } from 'app/interfaces/inmueble.interface';
import { User } from 'app/interfaces/user.interface';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ActivoService } from 'app/services/activo.service';
import { UserService } from 'app/services/user.service';
import Swal from 'sweetalert2';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-view-inmueble',
  templateUrl: './view-inmueble.component.html',
  styleUrls: ['./view-inmueble.component.scss']
})
export class ViewInmuebleComponent implements OnInit {

  @ViewChild('content') content: ElementRef;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginatorContactos: MatPaginator;
  @ViewChild(MatSort) sortContactos: MatSort;
  @ViewChild(MatPaginator) paginatorDocumentos: MatPaginator;
  @ViewChild(MatSort) sortDocumentos: MatSort;
  @ViewChild(MatPaginator) paginatorContratados: MatPaginator;
  @ViewChild(MatSort) sortContratados: MatSort;
  @ViewChild(MatPaginator) paginatorPublicos: MatPaginator;
  @ViewChild(MatSort) sortPublicos: MatSort;
  @ViewChild(MatPaginator) paginatorEventos: MatPaginator;
  @ViewChild(MatSort) sortEventos: MatSort;
  @ViewChild(MatPaginator) paginatorTodos: MatPaginator;
  @ViewChild(MatSort) sortTodos: MatSort;

  user: User;
  loader: boolean;
  panelOpenState = false;
  inmueble: Inmueble;
  contratoArrendamiento: any;
  edicion: boolean;
  monedaTypes: any[];
  periocidadTypes: any[];
  ingresos: MatTableDataSource<any>;
  ultimosIngresos: any[] = [];
  ultimosServiciosPublicos: any[] = [];
  ultimosServiciosContratados: any[] = [];
  ultimosContactos: any[] = [];
  ultimosEventos: any[] = [];
  ultimosTodos: any[] = [];
  contactos: MatTableDataSource<any>;
  documentos: MatTableDataSource<any>;
  obligacionesTypes: any[];
  impuesto: boolean;
  cuota: boolean;
  publicos: boolean;
  contratados: boolean;
  prestamos: boolean;
  poliza_seguro: boolean;
  nomSociedad: boolean;
  opciones: any[];
  sociedades: any[] = [];
  servicios_publicos: MatTableDataSource<any>;
  servicios_contratados: MatTableDataSource<any>;
  eventos: MatTableDataSource<any>;
  todos: MatTableDataSource<any>;
  displayedColumns: string[] = ['Tipo servicio', 'Fecha de pago', 'Periocidad', 'Numero referencia'];
  displayedColumnsIngresos: string[] = ['Nombre contrato', 'Nombre arrendatario', 'Canon', 'Moneda', 'Fecha de pago', 'Periocidad'];
  displayedColumnsContactos: string[] = ['Nombre', 'Empresa', 'Telefono', 'Celular', 'Correo'];
  displayedColumnsDocumentos: string[] = ['Nombre del documento', 'Acciones'];
  displayedColumnsEventos: string[] = ['Fecha', 'Nota'];
  displayedColumnsTodos: string[] = ['State', 'Nombre', 'Fecha'];
  impuestoPredial = {
    "fecha_pago": '',
    "periocidad": '',
    "valor_pago": 0,
    "tipo_moneda": ''
  };
  cuotaAdministracion = {
    "fecha_pago": '',
    "periocidad": '',
    "valor_pago": 0,
    "tipo_moneda": ''
  };
  prestamoDeuda = {
    "prestador": "",
    "fecha_pago": "",
    "periocidad": "",
    "valor_pago": 0,
    "tipo_moneda": '',
    "notas": ""
  };
  polizaSeguro = {
    "prestador": "",
    "fecha_pago": "",
    "periocidad": "",
    "valor_pago": 0,
    "tipo_moneda": '',
    "notas": ""
  }
  documento: File;
  nombreDocumento: string;
  colorBoton = '#bdbdbd';
  
  constructor(private activateRoute: ActivatedRoute, private activoService: ActivoService, public dialog: MatDialog,  private userService: UserService) { 
    this.loader = false;
    this.edicion = false;
    this.nomSociedad = false;
  }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((params: any) => {
      this.loader = true;
      this.infoInmueble(params._id);
    });
    this.asignacionSelects();
    this.getUser();
  }

  getUser() {
    this.userService.getUser().subscribe((user: User) => {
      this.user = user;
      if(this.user.sociedades.length > 0) {
        this.user.sociedades.forEach(element => {
          let obt = {
            value: element.razon_social, 
            viewValue: element.razon_social,
          }
          this.sociedades.push(obt);
        });
      } else {
        this.sociedades = [];
      }
    });
  }

  asignacionSelects() {

    this.monedaTypes = [
      {value: 'COP', viewValue: 'COP'},
      {value: 'USD', viewValue: 'USD'},
      {value: 'CAD', viewValue: 'CAD'},
      {value: 'EURO', viewValue: 'EURO  '},
    ]

    this.periocidadTypes = [
      {value: 'Diario', viewValue: 'Diario'},
      {value: 'Semanal', viewValue: 'Semanal'},
      {value: 'Mensual', viewValue: 'Mensual'},
      {value: 'Semestral', viewValue: 'Semestral'},
      {value: 'Anual', viewValue: 'Anual  '},
    ]

    this.opciones = [
      {value: 'Si', viewValue: 'Si'},
      {value: 'No', viewValue: 'No'},
    ]

  }

  infoInmueble(id: any) {
    this.activoService.getImueble(id)
      .subscribe((inmueble: Inmueble) => {
        this.inmueble = inmueble;
        console.log(this.inmueble)
        this.ingresos = new MatTableDataSource<any>(inmueble.ingresos);
        this.ultimosIngresos = this.inmueble.ingresos;
        this.eventos = new MatTableDataSource<any>(inmueble.eventos);
        this.ultimosEventos = this.inmueble.eventos;
        this.todos = new MatTableDataSource<any>(inmueble.todos);
        this.todos.filterPredicate = (data: any) => !data.state;
        this.todos.filter = 'excludeFalse';
        this.ultimosTodos = this.inmueble.todos;
        this.contactos = new MatTableDataSource<any>(inmueble.contactos);
        this.ultimosContactos = this.inmueble.contactos;
        this.documentos = new MatTableDataSource<any>(inmueble.documentos);
        this.servicios_publicos = new MatTableDataSource<any>(this.inmueble.servicios_publicos);
        this.ultimosServiciosPublicos = this.inmueble.servicios_publicos;
        this.servicios_contratados = new MatTableDataSource<any>(this.inmueble.servicios_contratados);
        this.ultimosServiciosContratados = this.inmueble.servicios_contratados;
        this.impuestoPredial = this.inmueble.impuesto_predial ? this.inmueble.impuesto_predial : this.impuestoPredial;
        this.cuotaAdministracion = this.inmueble.cuota_administracion ? this.inmueble.cuota_administracion : this.cuotaAdministracion;
        this.prestamoDeuda = this.inmueble.prestamos_deuda ? this.inmueble.prestamos_deuda : this.prestamoDeuda;
        this.polizaSeguro = this.inmueble.poliza_seguro_voluntario ? this.inmueble.poliza_seguro_voluntario : this.polizaSeguro;
        if (this.inmueble.sociedad_boolean === 'Si') {
          this.nomSociedad = true;
        }
        this.loader = false;
      })
  }

  ngAfterViewInit() {
    this.ingresos.paginator = this.paginator;
    this.ingresos.sort = this.sort;
    this.contactos.paginator = this.paginatorContactos;
    this.contactos.sort = this.sortContactos;
    this.documentos.paginator = this.paginatorDocumentos;
    this.documentos.sort = this.sortDocumentos;
    this.servicios_publicos.paginator = this.paginatorPublicos;
    this.servicios_publicos.sort = this.sortPublicos;
    this.servicios_contratados.paginator = this.paginatorContratados;
    this.servicios_contratados.sort = this.sortContratados;
    this.eventos.paginator = this.paginatorEventos;
    this.eventos.sort = this.sortEventos;
    this.todos.paginator = this.paginatorTodos;
    this.todos.sort = this.sortTodos;
  }

  applyFilterIngresos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.ingresos.filter = filterValue.trim().toLowerCase();

    if (this.ingresos.paginator) {
      this.ingresos.paginator.firstPage();
    }
  }

  applyFilterContactos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contactos.filter = filterValue.trim().toLowerCase();

    if (this.contactos.paginator) {
      this.contactos.paginator.firstPage();
    }
  }

  applyFilterDocumentos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  applyFilterServicios(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.servicios_publicos.filter = filterValue.trim().toLowerCase();

    if (this.servicios_publicos.paginator) {
      this.servicios_publicos.paginator.firstPage();
    }
  }

  applyFilterContratados(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.servicios_contratados.filter = filterValue.trim().toLowerCase();

    if (this.servicios_contratados.paginator) {
      this.servicios_contratados.paginator.firstPage();
    }
  }

  applyFilterEventos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.eventos.filter = filterValue.trim().toLowerCase();

    if (this.eventos.paginator) {
      this.eventos.paginator.firstPage();
    }
  }

  editFicha() {
    this.edicion = true;
    this.colorBoton = '#ef8354';
  }

  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'ingresos':
            this.ultimosIngresos.push(result);
            this.ingresos = new MatTableDataSource<any>(this.ultimosIngresos);
            break;
          case 'servpublicos':
            this.ultimosServiciosPublicos.push(result);
            this.servicios_publicos = new MatTableDataSource<any>(this.ultimosServiciosPublicos);
            break;
          case 'servcontratados':
            this.ultimosServiciosContratados.push(result);
            this.servicios_contratados = new MatTableDataSource<any>(this.ultimosServiciosContratados);
            break;
          case 'contactos':
            this.ultimosContactos.push(result);
            this.contactos = new MatTableDataSource<any>(this.ultimosContactos);
            break;
          case 'eventos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosEventos);
            this.ultimosEventos.push(result);
            this.eventos = new MatTableDataSource<any>(this.ultimosEventos);
            break;
          case 'todos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosTodos);
            this.ultimosTodos.push(result);
            this.todos = new MatTableDataSource<any>(this.ultimosTodos);
            break;
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            Swal.fire({
              title: 'Cargue de documentos',
              text: "Al cargar el documento se recargará la ventana y perderás la información editada en los anteriores ítems. Se recomienda primero guardar los datos anteriores. ¿Deseas continuar?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ef8354',
              cancelButtonColor: '#bdbdbd',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si'
            }).then((result) => {
              if (result.isConfirmed) {
                this.loader = true;
                this.activoService.uploadFileInmueble(this.inmueble._id, this.documento, this.nombreDocumento)
                .then(res => {
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Documento cargado exitosamente',
                    showConfirmButton: false,
                    timer: 2500
                  });
                  window.location.reload();
                })
                .catch((error) => {
                  Swal.fire('Error', error.error.error.message, 'error');
                });
              } else if (result.isDenied) {
                return;
              }
            })
            break;
        }
      } 
    });
  }

  onSociedadChange(sociedadBoolean: string) {
    switch(sociedadBoolean) {
      case 'Si':
        if(this.user.sociedades.length <= 0) {
          this.inmueble.sociedad_boolean = 'No';
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Aún no tienes sociedades creadas :(. Crea una sociedad para asignarla.'
          })
          this.nomSociedad = false;
        } else {
          this.nomSociedad = true;
          this.user.sociedades.forEach(element => {
            let obt = {
              value: element.razon_social, 
              viewValue: element.razon_social,
            }
            this.sociedades.push(obt);
          });
        }
        break;
      case 'No':
        this.nomSociedad = false;
        break;
    }
  }

  generateCsv() {
    const csvContent = "data:text/csv;charset=utf-8," + this.createCsvContent();
      
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', 'data.csv');
    document.body.appendChild(link);
    link.click();
  }

  private createCsvContent(): string {
    const headers = Object.keys(this.inmueble).join(',');
    const dataRow = Object.values(this.inmueble).join(',');
    return headers + '\n' + dataRow;
  }

  guardar() {
    this.inmueble.impuesto_predial = this.impuestoPredial;
    this.inmueble.cuota_administracion = this.cuotaAdministracion;
    this.inmueble.prestamos_deuda = this.prestamoDeuda;
    this.inmueble.poliza_seguro_voluntario = this.polizaSeguro;
    this.inmueble.ingresos = this.ultimosIngresos;
    this.inmueble.eventos = this.ultimosEventos;
    this.activoService.updateInmueble(this.inmueble._id, this.inmueble)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Inmueble Actualizado Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
        // this.router.navigateByUrl('what-have');
      }
    })
  }

}
