export interface PolizaSeguro {

    _id: string;
    subcategoria: string;
    nombre: string;
    aseguradora: string;
    nombre_de_intermediario: string;
    activo_persona_asegurada: string;
    notas: string;
    id_subtg: string;
    pago_prima: any[];
    contactos: any[];
    documentos: [];
    eventos: any[];
    todos: any[];
    reminders: any[];
}