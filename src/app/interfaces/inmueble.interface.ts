export interface Inmueble {

    _id: string;
    subcategoria: string;
    nombre: string;
    matricula_inmobiliaria: string;
    ubicacion: string;
    sociedad_boolean: string;
    nombre_sociedad: string;
    participacion: string;
    notas: boolean;
    ingresos: any[];
    impuesto_predial: any;
    cuota_administracion: any;
    servicios_publicos: [];
    servicios_contratados: [];
    prestamos_deuda: any;
    poliza_seguro_voluntario: any;
    contactos: [];
    documentos: [];
    eventos: any[];
    todos: any[];
    id_subtg: string;
    reminders: any[];
}