export interface  ProductoBancario {

    _id: string;
    subcategoria: string;
    nombre: string;
    ubicacion: string;
    entidad_financiera: string;
    num_producto: string;
    notas: string;
    id_subtg: string;
    pago_cuotas: any;
    contactos: any[];
    documentos: string[];

}