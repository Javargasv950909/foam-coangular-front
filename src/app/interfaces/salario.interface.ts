export interface Salario {

    _id: string;
    subcategoria: string;
    nombre: string;
    ubicacion: string;
    entidad_persona: string;
    valor: string;
    notas: string;
    id_subtg: string;
    ingreso_recurrente: any[];
    egreso_recurrente: any[];
    contactos: any[];
    documentos: [];

}