export interface Vehiculo {

    _id: string;
    subcategoria: string;
    nombre: string;
    tarjeta_propiedad: string;
    ubicacion: string;
    sociedad_boolean: string;
    nombre_sociedad: string;
    participacion: string;
    notas: boolean;
    ingresos: any[];
    impuesto_rodamiento: any;
    soat: any;
    tecnomecanica: any;
    poliza_seguro_voluntario: any;
    kit_carretera: any;
    estacionamiento: any;
    prestamos_deuda: any[];
    impuestos: [];
    contactos: [];
    documentos: [];
    id_subtg: string;
    eventos: any[];
    todos: any[];
    
}