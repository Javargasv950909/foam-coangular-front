export interface Prestamo {

    _id: string;
    subcategoria: string;
    nombre: string;
    ubicacion: string;
    entidad_persona: string;
    valor: number;
    notas: string;
    id_subtg: string;
    cobro_cuotas: any;
    pago_cuotas: any;
    contactos: any[];
    documentos: [];

}