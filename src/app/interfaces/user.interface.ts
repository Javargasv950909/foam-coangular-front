import { Evento } from "./evento.interface";
import {Inmueble } from "./inmueble.interface";
import { Inversion } from "./inversion.interface";
import { Membresia } from "./membresia.interface";
import { PolizaSeguro } from "./poliza_seguro.interface";
import { Prestamo } from "./prestamos.interface";
import { ProductoBancario } from "./pro_bancario.interface";
import { Salario } from "./salario.interface";
import { Sociedad } from "./sociedades.interface";
import { Vehiculo } from "./vehiculo.interface";

export interface User {
    _id?: string;
    first_name: string;
    second_name: string;
    firs_last_name: string;
    second_last_name: string;
    type_document: string;
    document_number: string;
    born_date: Date,
    number_phone: string;
    age: Number,
    email: string;
    isActive: boolean;
    documents: Array<any>;
    inmuebles: Array<Inmueble>;
    vehiculos: Array<Vehiculo>;
    sociedades: Array<Sociedad>;
    polizas_seguro: Array<PolizaSeguro>;
    inversiones: Array<Inversion>;
    productos_bancarios: Array<ProductoBancario>;
    prestamos: Array<Prestamo>;
    salarios: Array<Salario>;
    membresias: Array<Membresia>;
    reminders: Array<Evento>;
}