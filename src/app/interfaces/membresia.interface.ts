export interface Membresia {

    _id: string;
    subcategoria: string;
    nombre: string;
    ubicacion: string;
    entidad: string;
    notas: string;
    id_subtg: string;
    egreso_recurrente: any[];
    contactos: any[];
    documentos: string[];

}