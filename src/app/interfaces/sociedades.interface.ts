export interface Sociedad {

    _id: string;
    subcategoria: string;
    razon_social: string;
    ubicacion: string;
    nit: string;
    tipo_sociedad: string;
    notas: string;
    id_subtg: string;
    impuestos: any[];
    renovaciones_administrativas_documentarias: any[];
    obligaciones_laborales: any[];
    contactos: any[];
    documentos: any[];
    eventos: any[];
    todos: any[];

}