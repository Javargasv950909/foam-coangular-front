export interface Notificacion {
    _id: string;
    nombre: string;
    tipo_evento: string;
    concepto: string;
    fecha: Date;
    periocidad: string;
    state: any;
    valor?: string;
}