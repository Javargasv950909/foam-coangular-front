export interface Inversion {

    _id: string;
    subcategoria: string;
    nombre: string;
    ubicacion: string;
    entidad: string;
    valor_inversion_inicial: string;
    notas: string;
    id_subtg: string;
    intereses_dividendos: any[];
    contactos: any[];
    documentos: string[];
    eventos: any[];
    todos: any[];
    reminders: any[];

}