export interface Evento {
    // id: string,
    // title: string,
    // start: any,
    // end: any
    _id: string;
    nombre: string;
    tipo_evento: string;
    concepto: string;
    fecha: Date;
    periocidad: string;
    valor: string;
    state: boolean;
}