import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSalarioComponent } from './view-salario.component';

describe('ViewSalarioComponent', () => {
  let component: ViewSalarioComponent;
  let fixture: ComponentFixture<ViewSalarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewSalarioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewSalarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
