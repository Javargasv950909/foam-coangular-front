import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { User } from 'app/interfaces/user.interface';
import { Vehiculo } from 'app/interfaces/vehiculo.interface';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ActivoService } from 'app/services/activo.service';
import { UserService } from 'app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-vehiculo',
  templateUrl: './view-vehiculo.component.html',
  styleUrls: ['./view-vehiculo.component.scss']
})
export class ViewVehiculoComponent implements OnInit {

  @ViewChild(MatPaginator) paginatorIngresos: MatPaginator;
  @ViewChild(MatSort) sortIngresos: MatSort;
  @ViewChild(MatPaginator) paginatorContactos: MatPaginator;
  @ViewChild(MatSort) sortContactos: MatSort;
  @ViewChild(MatPaginator) paginatorDocumentos: MatPaginator;
  @ViewChild(MatSort) sortDocumentos: MatSort;
  @ViewChild(MatPaginator) paginatorImpuestos: MatPaginator;
  @ViewChild(MatSort) sortImpuestos: MatSort;
  @ViewChild(MatPaginator) paginatorPrestamos: MatPaginator;
  @ViewChild(MatSort) sortPrestamos: MatSort;
  @ViewChild(MatPaginator) paginatorEventos: MatPaginator;
  @ViewChild(MatSort) sortEventos: MatSort;
  @ViewChild(MatPaginator) paginatorTodos: MatPaginator;
  @ViewChild(MatSort) sortTodos: MatSort;

  user: User;
  loader: boolean;
  vehiculo: Vehiculo;
  ingresos: MatTableDataSource<any>;
  contactos: MatTableDataSource<any>;
  documentos: MatTableDataSource<any>;
  impuestos: MatTableDataSource<any>;
  prestamos: MatTableDataSource<any>;
  ultimosIngresos: any[] = [];
  ultimosContactos: any[] = [];
  ultimosImpuestos: any[] = [];
  ultimosPrestamos: any[] = [];
  ultimosEventos: any[] = [];
  ultimosTodos: any[] = [];
  eventos: MatTableDataSource<any>;
  todos: MatTableDataSource<any>;
  displayedColumnsImpuestos: string[] = ['Nombre impuesto', 'Fecha de pago', 'Periocidad', 'Valor', 'Tipo moneda'];
  displayedColumnsIngresos: string[] = ['Nombre del ingreso', 'Nombre arrendatario', 'valor', 'Moneda', 'Fecha de pago', 'Periocidad'];
  displayedColumnPrestamos: string[] = ['Nombre del prestador', 'Fecha de pago', 'Periocidad', 'Valor', 'Tipo moneda', 'Notas'];
  displayedColumnsContactos: string[] = ['Nombre', 'Empresa', 'Telefono', 'Celular', 'Correo'];
  displayedColumnsDocumentos: string[] = ['Nombre del documento', 'Acciones'];
  displayedColumnsEventos: string[] = ['Fecha', 'Nota'];
  displayedColumnsTodos: string[] = ['State', 'Nombre', 'Fecha'];
  edicion: boolean;
  documento: File;
  nombreDocumento: string;
  monedaTypes: any[];
  periocidadTypes: any[];
  opciones: any[];
  sociedades: any[] = [];
  nomSociedad: boolean;

  impuesto_rodamiento = {
    "fecha_pago": '',
    "periocidad": '',
    "valor_pago": 0,
    "tipo_moneda": ''
  };

  soat = {
    "fecha_pago": '',
    "periocidad": '',
    "valor_pago": 0,
    "tipo_moneda": ''
  };

  tecnomecanica = {
    "fecha_pago": '',
    "periocidad": '',
    "valor_pago": 0,
    "tipo_moneda": ''
  };

  poliza_seguro_voluntario = {
    "nombre_empresa": '',
    "fecha_pago": '',
    "periocidad": '',
    "valor_pago": 0,
    "tipo_moneda": ''
  };

  kit_carretera = {
    "fecha_vencimiento": '',
  };

  estacionamiento = {
    "fecha_pago": '',
    "periocidad": '',
    "valor_pago": 0,
    "tipo_moneda": ''
  };

  colorBoton = '#bdbdbd';

  constructor(private activateRoute: ActivatedRoute, private activoService: ActivoService, public dialog: MatDialog, private userService: UserService) { 
    this.loader = false;
    this.edicion = false;
    this.nomSociedad = false;
  }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((params: any) => {
      this.loader = true;
      this.infoVehiculo(params._id);
    });
    this.asignacionSelects();
    this.getUser();
  }

  asignacionSelects() {

    this.monedaTypes = [
      {value: 'COP', viewValue: 'COP'},
      {value: 'USD', viewValue: 'USD'},
      {value: 'CAD', viewValue: 'CAD'},
      {value: 'EURO', viewValue: 'EURO  '},
    ]

    this.periocidadTypes = [
      {value: 'Diario', viewValue: 'Diario'},
      {value: 'Semanal', viewValue: 'Semanal'},
      {value: 'Mensual', viewValue: 'Mensual'},
      {value: 'Semestral', viewValue: 'Semestral'},
      {value: 'Anual', viewValue: 'Anual  '},
    ]

    this.opciones = [
      {value: 'Si', viewValue: 'Si'},
      {value: 'No', viewValue: 'No'},
    ]

  }

  getUser() {
    this.userService.getUser().subscribe((user: User) => {
      this.user = user;
      if(this.user.sociedades.length > 0) {
        this.user.sociedades.forEach(element => {
          let obt = {
            value: element.razon_social, 
            viewValue: element.razon_social,
          }
          this.sociedades.push(obt);
        });
      } else {
        this.sociedades = [];
      }
    });
  }

  infoVehiculo(id: any) {
    this.activoService.getVehiculo(id)
      .subscribe((vehiculo: Vehiculo) => {
        this.vehiculo = vehiculo;
        this.ingresos = new MatTableDataSource<any>(vehiculo.ingresos);
        this.ultimosIngresos = this.vehiculo.ingresos;
        this.eventos = new MatTableDataSource<any>(vehiculo.eventos);
        this.ultimosEventos = this.vehiculo.eventos;
        this.todos = new MatTableDataSource<any>(vehiculo.todos);
        this.ultimosTodos = this.vehiculo.todos;
        this.todos.filterPredicate = (data: any) => !data.state;
        this.todos.filter = 'excludeFalse';
        this.contactos = new MatTableDataSource<any>(vehiculo.contactos);
        this.ultimosContactos = this.vehiculo.contactos;
        this.impuestos = new MatTableDataSource<any>(vehiculo.impuestos);
        this.ultimosImpuestos = this.vehiculo.impuestos;
        this.prestamos = new MatTableDataSource<any>(vehiculo.prestamos_deuda);
        this.ultimosPrestamos = this.vehiculo.prestamos_deuda;
        this.documentos = new MatTableDataSource<any>(vehiculo.documentos);
        this.impuesto_rodamiento = this.vehiculo.impuesto_rodamiento ? this.vehiculo.impuesto_rodamiento : this.impuesto_rodamiento;
        this.soat = this.vehiculo.soat ? this.vehiculo.soat : this.soat;
        this.tecnomecanica = this.vehiculo.tecnomecanica ? this.vehiculo.tecnomecanica : this.tecnomecanica;
        this.poliza_seguro_voluntario = this.vehiculo.poliza_seguro_voluntario ? this.vehiculo.poliza_seguro_voluntario : this.poliza_seguro_voluntario;
        this.kit_carretera = this.vehiculo.kit_carretera ? this.vehiculo.kit_carretera : this.kit_carretera;
        this.estacionamiento = this.vehiculo.estacionamiento ? this.vehiculo.estacionamiento : this.estacionamiento;
        if (this.vehiculo.sociedad_boolean === 'Si') {
          this.nomSociedad = true;
        }
        this.loader = false;
      })
  }

  ngAfterViewInit() {
    this.ingresos.paginator = this.paginatorIngresos;
    this.ingresos.sort = this.sortIngresos;
    this.contactos.paginator = this.paginatorContactos;
    this.contactos.sort = this.sortContactos;
    this.impuestos.paginator = this.paginatorImpuestos;
    this.impuestos.sort = this.sortImpuestos;
    this.prestamos.paginator = this.paginatorPrestamos;
    this.prestamos.sort = this.sortPrestamos;
    this.documentos.paginator = this.paginatorDocumentos;
    this.documentos.sort = this.sortDocumentos;
    this.eventos.paginator = this.paginatorEventos;
    this.eventos.sort = this.sortEventos;
    this.todos.paginator = this.paginatorTodos;
    this.todos.sort = this.sortTodos;
  }

  applyFilterIngresos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.ingresos.filter = filterValue.trim().toLowerCase();

    if (this.ingresos.paginator) {
      this.ingresos.paginator.firstPage();
    }
  }

  applyFilterContactos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contactos.filter = filterValue.trim().toLowerCase();

    if (this.contactos.paginator) {
      this.contactos.paginator.firstPage();
    }
  }

  applyFilterDocumentos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  applyFilterPrestamos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.prestamos.filter = filterValue.trim().toLowerCase();

    if (this.prestamos.paginator) {
      this.prestamos.paginator.firstPage();
    }
  }

  applyFilterImpuestos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.impuestos.filter = filterValue.trim().toLowerCase();

    if (this.impuestos.paginator) {
      this.impuestos.paginator.firstPage();
    }
  }

  applyFilterTodos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.todos.filter = filterValue.trim().toLowerCase();

    if (this.todos.paginator) {
      this.todos.paginator.firstPage();
    }
  }

  editFicha() {
    this.edicion = true;
    this.colorBoton = '#ef8354';
  }

  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'ingresosVehiculo':
            this.ultimosIngresos.push(result);
            this.ingresos = new MatTableDataSource<any>(this.ultimosIngresos);
            break;
          case 'impuestosVehiculo':
            this.ultimosImpuestos.push(result);
            this.impuestos = new MatTableDataSource<any>(this.ultimosImpuestos);
            break;
          case 'prestamosVehiculo':
            this.ultimosPrestamos.push(result);
            this.prestamos = new MatTableDataSource<any>(this.ultimosPrestamos);
            break;
          case 'contactos':
            this.ultimosContactos.push(result);
            this.contactos = new MatTableDataSource<any>(this.ultimosContactos);
            break;
          case 'eventos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosEventos);
            this.ultimosEventos.push(result);
            this.eventos = new MatTableDataSource<any>(this.ultimosEventos);
            break;
          case 'todos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosTodos);
            this.ultimosTodos.push(result);
            this.todos = new MatTableDataSource<any>(this.ultimosTodos);
            break;
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            Swal.fire({
              title: 'Cargue de documentos',
              text: "Al cargar el documento se recargará la ventana y perderás la información editada en los anteriores ítems. Se recomienda primero guardar los datos anteriores. ¿Deseas continuar?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ef8354',
              cancelButtonColor: '#bdbdbd',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si'
            }).then((result) => {
              if (result.isConfirmed) {
                this.loader = true;
                this.activoService.uploadFileVehiculo(this.vehiculo._id, this.documento, this.nombreDocumento)
                .then(res => {
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Documento cargado exitosamente',
                    showConfirmButton: false,
                    timer: 2500
                  });
                  window.location.reload();
                })
                .catch((error) => {
                  Swal.fire('Error', error.error.error.message, 'error');
                });
              } else if (result.isDenied) {
                return;
              }
            })
            break;
        }
      } 
    });
  }

  onSociedadChange(sociedadBoolean: string) {
    switch(sociedadBoolean) {
      case 'Si':
        if(this.user.sociedades.length <= 0) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Aún no tienes sociedades creadas :(. Crea una sociedad para asignarla.'
          })
          this.nomSociedad = false;
          this.vehiculo.sociedad_boolean = 'No';
        } else {
          this.nomSociedad = true;
          this.user.sociedades.forEach(element => {
            let obt = {
              value: element.razon_social, 
              viewValue: element.razon_social,
            }
            this.sociedades.push(obt);
          });
        }
        break;
      case 'No':
        this.nomSociedad = false;
        break;
    }
  }

  guardar() {
    this.vehiculo.impuesto_rodamiento = this.impuesto_rodamiento;
    this.vehiculo.soat = this.soat;
    this.vehiculo.tecnomecanica = this.tecnomecanica;
    this.vehiculo.poliza_seguro_voluntario = this.poliza_seguro_voluntario;
    this.vehiculo.kit_carretera = this.kit_carretera;
    this.vehiculo.estacionamiento = this.estacionamiento;
    this.vehiculo.ingresos = this.ultimosIngresos;
    this.vehiculo.prestamos_deuda = this.ultimosPrestamos;
    this.vehiculo.eventos = this.ultimosEventos;
    this.activoService.updateVehiculo(this.vehiculo._id, this.vehiculo)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Vehiculo Actualizado Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
        // this.router.navigateByUrl('what-have');
      }
    })
  }


}
