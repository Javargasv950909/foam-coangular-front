import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CashformatPipe } from 'app/pipes/cashformat.pipe';
import { ActivoService } from 'app/services/activo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-calendar',
  templateUrl: './modal-calendar.component.html',
  styleUrls: ['./modal-calendar.component.scss']
})
export class ModalCalendarComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  loader: boolean;
  activo: any;
  reminders: MatTableDataSource<any>;
  displayedColumns: string[] = ['Nombre', 'Tipo evento', 'Fecha', 'Periocidad', 'Acciones'];
  periocidadTypes: any[];
  tipoEventos: any[];
  opciones: any[];
  documentos: boolean;
  isReminder: boolean;
  isNew: boolean;
  reminder: any;

  constructor(public dialogRef: MatDialogRef<ModalCalendarComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private activeService: ActivoService, private currencyPipe: CashformatPipe) {
    this.activo = data;
    this.loader = false;
    this.isNew = false;
  }

  public reminder_form = this.fb.group({
    nombre: ['', Validators.required],
    tipo_evento: ['', Validators.required],
    fecha: ['', Validators.required],
    periocidad: ['', Validators.required],
    todo: [''],
    valor: [''],
    state: false
  })

  ngOnInit(): void {
    this.loader = true;
    this.periocidadTypes = [
      {value: 'Diario', viewValue: 'Diario'},
      {value: 'Semanal', viewValue: 'Semanal'},
      {value: 'Mensual', viewValue: 'Mensual'},
      {value: 'Semestral', viewValue: 'Semestral'},
      {value: 'Anual', viewValue: 'Anual'},
      {value: 'No se repite', viewValue: 'No se repite'},
    ]
    this.tipoEventos = [
      {value: 'Ingreso', viewValue: 'Ingreso'},
      {value: 'Obligacion', viewValue: 'Obligacion'},
      {value: 'Pago', viewValue: 'Pago'},
      {value: 'Vencimiento', viewValue: 'Vencimiento'},
      {value: 'Otro', viewValue: 'Otro'},
    ];
    this.opciones = [
      {value: 'Si', viewValue: 'Si'},
      {value: 'No', viewValue: 'No'}
    ];
    if(this.activo === 'new') {
      this.isReminder = false;
      this.documentos = false;
      this.isNew = true;
      this.loader = false;
    } else {
      this.asignarPerfil(this.activo.id_subtg);
    }
  }

  asignarPerfil(id_subtg: string) {
    switch(id_subtg) {
      case '1':
        this.activeService.getImueble(this.activo._id)
        .subscribe((inmueble: any) => {
          this.reminders = new MatTableDataSource<any>(inmueble.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '2':
        this.activeService.getVehiculo(this.activo._id)
        .subscribe((vehiculo: any) => {
          this.reminders = new MatTableDataSource<any>(vehiculo.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '3':
        this.activeService.getSociedad(this.activo._id)
        .subscribe((sociedad: any) => {
          this.reminders = new MatTableDataSource<any>(sociedad.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '4':
        this.activeService.getPoliza(this.activo._id)
        .subscribe((poliza: any) => {
          this.reminders = new MatTableDataSource<any>(poliza.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '5':
        this.activeService.getInversion(this.activo._id)
        .subscribe((inversion: any) => {
          this.reminders = new MatTableDataSource<any>(inversion.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '6':
        this.activeService.getPbancario(this.activo._id)
        .subscribe((producto: any) => {
          this.reminders = new MatTableDataSource<any>(producto.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '7':
        this.activeService.getPrestamo(this.activo._id)
        .subscribe((prestamo: any) => {
          this.reminders = new MatTableDataSource<any>(prestamo.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '8':
        this.activeService.getSalario(this.activo._id)
        .subscribe((salario: any) => {
          this.reminders = new MatTableDataSource<any>(salario.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '9':
        this.activeService.getMembresia(this.activo._id)
        .subscribe((membresia: any) => {
          this.reminders = new MatTableDataSource<any>(membresia.reminders);
          this.documentos = false;
          this.loader = false;
        })
        break;
      case '10':
        this.activeService.getDocumento(this.activo._id)
        .subscribe((documento: any) => {
          if(documento.reminder) {
            this.isReminder = true;
            this.reminder = documento.reminder;
          } else {
            this.isReminder = false;
            this.reminder = {
              nombre: '',
              tipo_evento: '',
              concepto: '',
              fecha: '',
              periocidad: '',
              valor: ''
            }
          }
          this.documentos = true;
          this.loader = false;
        })
        break;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.reminders.filter = filterValue.trim().toLowerCase();

    if (this.reminders.paginator) {
      this.reminders.paginator.firstPage();
    }
  }

  ngAfterViewInit() {
    this.reminders.paginator = this.paginator;
    this.reminders.sort = this.sort;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  deleteReminder(reminder: any) {
    this.activeService.deleteReminder(reminder._id)
    .subscribe({
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Recuerdame Eliminado Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
        this.reminder_form.reset();
        this.dialogRef.close();
      }
    })

  }

  saveReminder() {
    if(this.isNew) {
        this.activeService.newReminderforUser(this.reminder_form.value)
          .subscribe({
            error: (e:any) => {
              Swal.fire('Error', e.error.error.message, 'error');
            },
            next: (res) => {
              Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Recuerdame Agregado Exitosamente',
                showConfirmButton: false,
                timer: 2500
              });
              this.reminder_form.reset();
              this.dialogRef.close(this.isNew);
            }
          })
      } else {
        this.activeService.newReminderforActive(this.activo._id, this.activo.id_subtg, this.reminder_form.value)
        .subscribe({
          error: (e:any) => {
            Swal.fire('Error', e.error.error.message, 'error');
          },
          next: (res) => {
            Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Recuerdame Agregado Exitosamente',
              showConfirmButton: false,
              timer: 2500
            });
            this.reminder_form.reset();
            this.dialogRef.close();
          }
        })
    }
  }

  formatCurrency(event: any): void {
    let inputValue = event.target.value;

    // Remover caracteres no numéricos
    inputValue = inputValue.replace(/[^0-9]/g, '');

    // Convertir a número y dividir por 100 para obtener el formato adecuado
    const numericValue = parseFloat(inputValue) / 100;

    // Aplicar el formato de moneda COP
    const formattedValue = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
    }).format(numericValue);

    // Actualizar el valor del campo con el formato de moneda
    this.reminder_form.get('valor').setValue(formattedValue, { emitEvent: false });
  }

}
