import { Component, inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'app/interfaces/user.interface';
import { UserService } from 'app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-active',
  templateUrl: './new-active.component.html',
  styleUrls: ['./new-active.component.scss']
})
export class NewActiveComponent implements OnInit {

  user: User;
  formSubmitted: boolean = false;
  categorias: any[];
  categoriaSeleccionada: string;
  subcategorias: any[];
  opciones: any[];
  dataAdicional: any[];
  seleccionSub: boolean;
  filas: any[];
  selectActive: boolean;
  nomSociedad: boolean;
  sociedades: any[] = [];
  loader: boolean;

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService ) {
    this.seleccionSub = false;
    this.selectActive = false;
    this.nomSociedad = false;
    this.loader = false;
  }

  public activoForm = this.fb.group({
    subcategoria: ['', Validators.required],
    sociedad_boolean: [''],
    nombre_sociedad: [''],
    participacion: [''],
    id_subtg: ['']
  });

  ngOnInit(): void {  
    this.loader = true;
    this.asignarData();
  }

  asignarData() {
    this.userService.getUser().subscribe((user: User) => {
      this.user = user;
    })

    this.categorias = [
      {value: 'Bienes inmuebles', viewValue: 'Bienes inmuebles'},
      {value: 'Vehiculos', viewValue: 'Vehiculos'},
      {value: 'Sociedades', viewValue: 'Sociedades'},
      {value: 'Polizas de Seguro', viewValue: 'Polizas de Seguro'},
      {value: 'Inversiones', viewValue: 'Inversiones'},
      {value: 'Productos bancarios', viewValue: 'Productos Bancarios'},
      {value: 'Prestamos', viewValue: 'Prestamos'},
      {value: 'Salarios', viewValue: 'Salarios'},
      {value: 'Membresias/Suscripciones', viewValue: 'Membresias/Suscripciones'},
    ]

    this.opciones = [
      {value: 'Si', viewValue: 'Si'},
      {value: 'No', viewValue: 'No'},
    ]
    this.loader = false;
  }
                                                                                                                                        
  onCategoriaChange(categoriaValue: string) {
    this.seleccionSub = false;
    switch(categoriaValue) {
      case 'Bienes inmuebles':
        this.categoriaSeleccionada = 'Bienes inmuebles';
        this.subcategorias = [
          {value: 'Apartamento', viewValue: 'Apartamento'},
          {value: 'Casa', viewValue: 'Casa'},
          {value: 'Finca', viewValue: 'Finca'},
          {value: 'Oficina', viewValue: 'Oficina'},
          {value: 'Local', viewValue: 'Local'},
          {value: 'Bodega', viewValue: 'Bodega'},
          {value: 'Lote', viewValue: 'Lote'}
        ];
        break;
      case 'Vehiculos':
        this.categoriaSeleccionada = 'Vehiculos';
        this.subcategorias = [
          {value: 'Carro', viewValue: 'Carro'},
          {value: 'Moto', viewValue: 'Moto'},
          {value: 'Camion', viewValue: 'Camion'},
          {value: 'Avion', viewValue: 'Avion'},
          {value: 'Lancha/Yate', viewValue: 'Lancha/Yate'},
          {value: 'Bicicleta', viewValue: 'Bicicleta'},
          {value: 'Patineta', viewValue: 'Patineta'}
        ];
        break;
      case 'Sociedades':
        this.categoriaSeleccionada = 'Sociedades';
        this.subcategorias = [
          {value: 'Sociedades Generales', viewValue: 'Sociedades Generales'}
        ];
        break;
      case 'Polizas de Seguro':
        this.categoriaSeleccionada = 'Polizas de seguro';
        this.subcategorias = [
          {value: 'Seguro de vida', viewValue: 'Seguro de vida'},
          {value: 'Seguro de salud', viewValue: 'Seguro de salud'},
        ];
        break;
      case 'Inversiones':
        this.categoriaSeleccionada = 'Inversiones';
        this.subcategorias = [
          {value: 'CDT/Depositos a terminos', viewValue: 'CDT/Depositos a terminos'},
          {value: 'Acciones/Indices', viewValue: 'Acciones/Indices'},
          {value: 'Fondos de inversion', viewValue: 'Fondos de inversion'},
          {value: 'Otras empresas', viewValue: 'Otras empresas'},
          {value: 'Otras inversiones', viewValue: 'Otras inversiones'},
        ];
        break;
      case 'Productos bancarios':
        this.categoriaSeleccionada = 'Productos bancarios';
        this.subcategorias = [
          {value: 'Cuenta de ahorro/corriente', viewValue: 'Cuenta de ahorro/corriente'},
          {value: 'Tarjeta de credito', viewValue: 'Tarjeta de credito'},
        ];
        break;
      case 'Prestamos':
        this.categoriaSeleccionada = 'Prestamos';
        this.subcategorias = [
          {value: 'Prestamos recibidos', viewValue: 'Prestamos recibidos'},
          {value: 'Prestamos otorgados', viewValue: 'Prestamos otorgados'},
        ];
        break;
      case 'Salarios':
        this.categoriaSeleccionada = 'Salarios';
        this.subcategorias = [
          {value: 'Salario por recibir', viewValue: 'Salario por recibir'},
          {value: 'Salario por pagar', viewValue: 'Salario por pagar'},
        ];
        break;
      case 'Membresias/Suscripciones':
        this.categoriaSeleccionada = 'Membresias/Suscripciones';
        this.subcategorias = [
          {value: 'Clubes y Asociaciones', viewValue: 'Clubes y Asociaciones'},
          {value: 'Suscripciones digitales', viewValue: 'Suscripciones digitales'},
          {value: 'Gimnasio', viewValue: 'Gimnasio'},
          {value: 'Periodicos y revistas', viewValue: 'Periodicos y revistas'},
        ];
        break;
    }
    // Resetea el valor seleccionado de la subcategoría
    this.activoForm.get('subcategoria').setValue('');
  }

  onSubCategoriaChange() {
    this.seleccionSub = true;
    switch(this.categoriaSeleccionada) {
      case 'Bienes inmuebles':
        this.selectActive = true;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre/Referencia'},
          {value: 'matricula_inmobiliaria', viewValue: 'Matricula inmobiliriaria '},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Vehiculos':
        this.selectActive = true;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre/Referencia'},
          {value: 'tarjeta_propiedad', viewValue: 'Tarjeta de Propiedad '},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Sociedades':
        this.selectActive = false;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'razon_social', viewValue: 'Razon Social'},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'nit', viewValue: 'NIT o Registro Fiscal'},
          {value: 'tipo_sociedad', viewValue: 'Tipo de Sociedad'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Polizas de seguro':
        this.selectActive = false;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre / Referencia'},
          {value: 'aseguradora', viewValue: 'Compañia aseguradora '},
          {value: 'nombre_de_intermediario', viewValue: 'Nombre de Intermediario'},
          {value: 'activo_persona_asegurada', viewValue: 'Activo o Persona Asegurada'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Inversiones':
        this.selectActive = false;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre / Referencia'},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'entidad', viewValue: 'Entidad '},
          {value: 'valor_inversion_inicial', viewValue: 'Valor de inversion inicial'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Productos bancarios':
        this.selectActive = false;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre / Referencia'},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'entidad_financiera', viewValue: ' Entidad financiera'},
          {value: 'num_producto', viewValue: 'Numero de producto '},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Prestamos':
        this.selectActive = false;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre / Referencia'},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'entidad_persona', viewValue: 'Entidad o persona '},
          {value: 'valor', viewValue: 'Valor'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Salarios':
        this.selectActive = false;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre / Referencia'},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'entidad_persona', viewValue: 'Entidad o persona '},
          {value: 'valor', viewValue: 'Valor'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
      case 'Membresias/Suscripciones':
        this.selectActive = false;
        this.dataAdicional =  [];
        this.dataAdicional = [
          {value: 'nombre', viewValue: 'Nombre / Referencia'},
          {value: 'ubicacion', viewValue: 'Ubicacion'},
          {value: 'entidad', viewValue: 'Entidad'},
          {value: 'notas', viewValue: 'Notas'}
        ];  
        break;
    }

    for (const data of this.dataAdicional) {
      this.activoForm.addControl(data.value, new FormControl(''));
    }
    
    this.obtenerFilas();
  }

  onSociedadChange(sociedadBoolean: string) {
    switch(sociedadBoolean) {
      case 'Si':
        if(this.user.sociedades.length <= 0) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Aún no tienes sociedades creadas :(. Crea una sociedad para asignarla.'
          })
          this.nomSociedad = false;
          this.activoForm.controls['sociedad_boolean'].setValue('No');
        } else {
          this.nomSociedad = true;
          this.user.sociedades.forEach(element => {
            let obt = {
              value: element.razon_social, 
              viewValue: element.razon_social,
            }
            this.sociedades.push(obt);
          });
        }
        break;
      case 'No':
        this.nomSociedad = false;
        break;
    }
  }

  obtenerFilas() {
    const cantidadElementos = this.dataAdicional.length;
    const cantidadFilas = Math.ceil(cantidadElementos / 2);
    this.filas = [];

    for (let i = 0; i < cantidadFilas; i++) {
      const fila = this.dataAdicional.slice(i * 2, i * 2 + 2);
      this.filas.push(fila);
    }
  }

  create() {
    this.formSubmitted = true;
    if(this.activoForm.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Todos los campos con * son obligatorios. Por favor completarlos.'
      })
    } else {
      switch(this.categoriaSeleccionada) {
        case 'Bienes inmuebles':
          this.activoForm.value.id_subtg = '1';
          this.post(this.activoForm.value);
          break;
        case 'Vehiculos':
          this.activoForm.value.id_subtg = '2';
          this.post(this.activoForm.value);
          break;
        case 'Sociedades':
          this.activoForm.value.id_subtg = '3';
          this.post(this.activoForm.value);
          break;
        case 'Polizas de seguro':
          this.activoForm.value.id_subtg = '4';
          this.post(this.activoForm.value);
          break;
        case 'Inversiones':
          this.activoForm.value.id_subtg = '5';
          this.post(this.activoForm.value);
          break;
        case 'Productos bancarios':
          this.activoForm.value.id_subtg = '6';
          this.post(this.activoForm.value);
          break;
        case 'Prestamos':
          this.activoForm.value.id_subtg = '7';
          this.post(this.activoForm.value);
          break;
        case 'Salarios':
          this.activoForm.value.id_subtg = '8';
          this.post(this.activoForm.value);
          break;
        case 'Membresias/Suscripciones':
          this.activoForm.value.id_subtg = '9';
          this.post(this.activoForm.value);
          break;
      }
    }
  }

  post(formvalue: any) {
   return this.userService.newActivo(this.categoriaSeleccionada, formvalue)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Registro Exitoso',
          showConfirmButton: false,
          timer: 2500
        });
        this.activoForm.reset();
        this.router.navigateByUrl('what-have');
      }
    })
  }

}
