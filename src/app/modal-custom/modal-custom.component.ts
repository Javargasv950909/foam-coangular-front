import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { CashformatPipe } from 'app/pipes/cashformat.pipe';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-custom',
  templateUrl: './modal-custom.component.html',
  styleUrls: ['./modal-custom.component.scss']
})
export class ModalCustomComponent implements OnInit{

  path: any;
  periocidadTypes: any[];
  serviciosPTypes: any[];
  serviciosCTypes: any[];
  ingresos: boolean;
  ingresosVehiculo: boolean;
  impuestosVehiculo: boolean;
  prestamosVehiculo: boolean;
  servicios_publicos: boolean;
  servicios_contratados: boolean;
  impuestosSociedad: boolean;
  renovacionesSociedad: boolean;
  obligacionesSociedad: boolean;
  pagosPrimas: boolean;
  ingresosSalario: boolean;
  egresosRecurrente: boolean;
  contactos: boolean;
  files: boolean;
  title: string;
  monedaTypes: any[];
  archivoSubir: File;
  categorias: any[];
  subcategorias: any[];
  seleccionSub: boolean;
  eventos: boolean;
  todos: boolean;
  tipoEventos: any[];
  opciones: any[];

  public ingreso_form = this.fb.group({
    nombre_contrato: ['', Validators.required],
    nombre_arrendatario: ['', Validators.required],
    canon: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required]
  })

  public servicio_publico_form = this.fb.group({
    tipo_servicio: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    num_referencia: ['', Validators.required]
  })

  public servicio_contratado_form = this.fb.group({
    tipo_servicio: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    num_referencia: ['', Validators.required]
  })

  public ingresoVehiculo_form = this.fb.group({
    nombre_ingreso: ['', Validators.required],
    nombre_arrendatario: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required]
  })

  public impuestoVehiculo_form = this.fb.group({
    nombre_impuesto: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required]
  })

  public prestamoVehiculo_form = this.fb.group({
    prestador: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    valor_pago: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required],
    notas: ['', Validators.required]
  })

  public impuestoSociedad_form = this.fb.group({
    nombre_impuesto: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required]
  })

  public renovacionSociedad_form = this.fb.group({
    nombre: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required]
  })

  public obligacionSociedad_form = this.fb.group({
    nombre: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required]
  })

  public pagosPrimas_form = this.fb.group({
    nombre: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required]
  })

  public ingresoRecurrente_form = this.fb.group({
    nombre: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required]
  })

  public egresoRecurrente_form = this.fb.group({
    nombre: ['', Validators.required],
    fecha_pago: ['', Validators.required],
    periocidad: ['', Validators.required],
    valor: ['', Validators.required],
    tipo_moneda: ['COP', Validators.required]
  })

  public contactos_form = this.fb.group({
    nombre: ['', Validators.required],
    empresa: ['', Validators.required],
    telefono: ['', Validators.required],
    celular: ['', Validators.required],
    correo: ['', Validators.required]
  })

  public files_form = this.fb.group({
    nombre: ['', Validators.required],
    file: [File, Validators.required],
    categoria: ['', Validators.required],
    subcategoria: ['', Validators.required],
    documento: [''],
    notas: ['']
  })

  public eventos_form = this.fb.group({
    nota: ['', Validators.required],
    fecha: ['', Validators.required]
  })

  public todos_form = this.fb.group({
    nombre: ['', Validators.required],
    fecha: ['', Validators.required],
    state: false
  })


  constructor(public dialogRef: MatDialogRef<ModalCustomComponent>, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.path = data;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.asignarFormulario();
    this.periocidadTypes = [
      {value: 'Diario', viewValue: 'Diario'},
      {value: 'Semanal', viewValue: 'Semanal'},
      {value: 'Mensual', viewValue: 'Mensual'},
      {value: 'Semestral', viewValue: 'Semestral'},
      {value: 'Anual', viewValue: 'Anual  '},
      {value: 'No se repite', viewValue: 'No se repite'},
    ]
    this.tipoEventos = [
      {value: 'Ingreso', viewValue: 'Ingreso'},
      {value: 'Obligacion', viewValue: 'Obligacion'},
      {value: 'Pago', viewValue: 'Pago'},
      {value: 'Vencimiento', viewValue: 'Vencimiento'},
      {value: 'Otro', viewValue: 'Otro'},
    ];
    this.opciones = [
      {value: 'Si', viewValue: 'Si'},
      {value: 'No', viewValue: 'No'}
    ];
    this.serviciosPTypes = [
      {value: 'Agua', viewValue: 'Agua'},
      {value: 'Luz', viewValue: 'Luz'},
      {value: 'Gas', viewValue: 'Gas'},
      {value: 'Telefono', viewValue: 'Telefono'},
      {value: 'Internet', viewValue: 'Internet'},
      {value: 'Cable', viewValue: 'Cable'},
      {value: 'Otro', viewValue: 'Otro'},
    ]
    this.serviciosCTypes = [
      {value: 'Seguridad', viewValue: 'Seguridad'},
      {value: 'Mantenimiento', viewValue: 'Mantenimiento'},
      {value: 'Jardinero', viewValue: 'Jardinero'},
      {value: 'Administrativo', viewValue: 'Administrativo'},
      {value: 'Otro', viewValue: 'Otro'},
    ]
    this.monedaTypes = [
      {value: 'COP', viewValue: 'COP'},
      {value: 'USD', viewValue: 'USD'},
      {value: 'CAD', viewValue: 'CAD'},
      {value: 'EURO', viewValue: 'EURO  '},
    ]
    this.categorias = [
      {value: 'Identificacion Personal', viewValue: 'Identificacion Personal'},
      {value: 'Registro Civil', viewValue: 'Registro Civil'}
    ]
  }

  onCategoriaChange(categoriaValue: string) {
    this.seleccionSub = false;
    switch(categoriaValue) {
      case 'Identificacion Personal':
        this.subcategorias = [
          {value: 'Documentos de Identidad', viewValue: 'Documentos de Identidad'},
          {value: 'Licencias de Conduccion', viewValue: 'Licencias de Conduccion'},
          {value: 'Pasaportes', viewValue: 'Pasaportes'},
          {value: 'Titulos Academicos', viewValue: 'Titulos Academicos'},
          {value: 'Libreta Militar', viewValue: 'Libreta Militar'},
          {value: 'Tarjetas Profesionales', viewValue: 'Tarjetas Profesionales'},
          {value: 'Visas', viewValue: 'Visas'},
          {value: 'Vacunas', viewValue: 'Vacunas'}
        ];
        break;
      case 'Registro Civil':
        this.subcategorias = [
          {value: 'Actas de Nacimiento', viewValue: 'Actas de Nacimiento'},
          {value: 'Actas de Bautizo', viewValue: 'Actas de Bautizo'},
          {value: 'Acta de Matrimonio', viewValue: 'Acta de Matrimonio'},
          {value: 'Acta de Defuncion', viewValue: 'Acta de Defuncion'}
        ];
        break;
    }
    // Resetea el valor seleccionado de la subcategoría
    this.files_form.get('subcategoria').setValue('');
  }

  asignarFormulario() {
    switch(this.path) {
      case 'ingresos':
        this.title = 'Ingreso';
        this.ingresos = true;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'servpublicos':
        this.title = 'Servicio Público';
        this.ingresos = false;
        this.servicios_publicos = true;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'servcontratados':
        this.title = 'Servicio Contratado';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = true;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'ingresosVehiculo':
        this.title = 'Ingreso vehicular';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = true;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'impuestosVehiculo':
        this.title = 'Ingreso vehicular';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = true;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'prestamosVehiculo':
        this.title = 'Prestamos / Deuda ';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = true;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'impuestosSociedad':
        this.title = 'Impuesto';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = true;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'renovacionesSociedad':
        this.title = 'Renovacion administrativa';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = true;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'obligacionesSociedad':
        this.title = 'Obligacion Laboral';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = true;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'pagosPrimas':
        this.title = 'Pago de prima';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = true;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'ingresosSalario':
        this.title = 'Ingreso recurrente';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = true;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'egresosRecurrente':
        this.title = 'Egreso recurrente';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = true;
        this.eventos = false;
        this.todos = false;
        break;
      case 'contactos':
        this.title = 'Contacto';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = true;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'files':
        this.title = 'Documento';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = true;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = false;
        break;
      case 'eventos':
        this.title = 'Evento';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = true;
        this.todos = false;
        break;
      case 'todos':
        this.title = 'To-Do';
        this.ingresos = false;
        this.servicios_publicos = false;
        this.servicios_contratados = false;
        this.contactos = false;
        this.files = false;
        this.ingresosVehiculo = false;
        this.impuestosVehiculo = false;
        this.prestamosVehiculo = false;
        this.impuestosSociedad = false;
        this.renovacionesSociedad = false;
        this.obligacionesSociedad = false;
        this.pagosPrimas = false;
        this.ingresosSalario = false;
        this.egresosRecurrente = false;
        this.eventos = false;
        this.todos = true;
        break;
  }
  }

  cargarFile(file: File) {
    this.archivoSubir = file;
    if(! file) { return; }
  }

  showErrorPopUp() {
    return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Todos los campos con * son obligatorios. Por favor completarlos.'
      })
  }

  adicionar() {
    switch(this.path) {
      case 'ingresos':
        if(this.ingreso_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoIngresos = {
            nombre_contrato: this.ingreso_form.value.nombre_contrato,
            nombre_arrendatario: this.ingreso_form.value.nombre_arrendatario,
            canon: this.ingreso_form.value.canon,
            tipo_moneda: this.ingreso_form.value.tipo_moneda,
            fecha_pago: this.ingreso_form.value.fecha_pago,
            periocidad: this.ingreso_form.value.periocidad
          }
          this.dialogRef.close(objetoIngresos);
        }
        break;
      case 'ingresosVehiculo':
        if(this.ingresoVehiculo_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoIngresoVehicular = {
            nombre_ingreso: this.ingresoVehiculo_form.value.nombre_ingreso,
            nombre_arrendatario: this.ingresoVehiculo_form.value.nombre_arrendatario,
            valor: this.ingresoVehiculo_form.value.valor,
            tipo_moneda: this.ingresoVehiculo_form.value.tipo_moneda,
            fecha_pago: this.ingresoVehiculo_form.value.fecha_pago,
            periocidad: this.ingresoVehiculo_form.value.periocidad
          }
          this.dialogRef.close(objetoIngresoVehicular);
        }
        break;
      case 'prestamosVehiculo':
        if(this.prestamoVehiculo_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoPrestamoVehicular = {
            prestador: this.prestamoVehiculo_form.value.prestador,
            fecha_pago: this.prestamoVehiculo_form.value.fecha_pago,
            periocidad: this.prestamoVehiculo_form.value.periocidad,
            valor_pago: this.prestamoVehiculo_form.value.valor_pago,
            tipo_moneda: this.prestamoVehiculo_form.value.tipo_moneda,
            notas: this.prestamoVehiculo_form.value.notas
          }
          this.dialogRef.close(objetoPrestamoVehicular);
        }
        break;
      case 'impuestosVehiculo':
        if(this.impuestoVehiculo_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoImpuestoVehicular = {
            nombre_impuesto: this.impuestoVehiculo_form.value.nombre_impuesto,
            valor: this.impuestoVehiculo_form.value.valor,
            tipo_moneda: this.impuestoVehiculo_form.value.tipo_moneda,
            fecha_pago: this.impuestoVehiculo_form.value.fecha_pago,
            periocidad: this.impuestoVehiculo_form.value.periocidad
          }
          this.dialogRef.close(objetoImpuestoVehicular);
        }
        break;
      case 'servpublicos':
        if(this.servicio_publico_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoSP = {
            tipo_servicio: this.servicio_publico_form.value.tipo_servicio,
            fecha_pago: this.servicio_publico_form.value.fecha_pago,
            periocidad: this.servicio_publico_form.value.periocidad,
            num_referencia: this.servicio_publico_form.value.num_referencia
          }
          this.dialogRef.close(objetoSP);
        }
        break;
      case 'servcontratados':
        if(this.servicio_contratado_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoSC = {
            tipo_servicio: this.servicio_contratado_form.value.tipo_servicio,
            fecha_pago: this.servicio_contratado_form.value.fecha_pago,
            periocidad: this.servicio_contratado_form.value.periocidad,
            num_referencia: this.servicio_contratado_form.value.num_referencia
          }
          this.dialogRef.close(objetoSC);
        }
        break;
      case 'impuestosSociedad':
        if(this.impuestoSociedad_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objeto = {
            nombre_impuesto: this.impuestoSociedad_form.value.nombre_impuesto,
            fecha_pago: this.impuestoSociedad_form.value.fecha_pago,
            periocidad: this.impuestoSociedad_form.value.periocidad,
            valor: this.impuestoSociedad_form.value.valor,
            tipo_moneda: this.impuestoSociedad_form.value.tipo_moneda
          }
          this.dialogRef.close(objeto);
        }
        break;
      case 'renovacionesSociedad':
          if(this.renovacionSociedad_form.invalid) {
            this.showErrorPopUp();
          } else {
            let objeto = {
              nombre: this.renovacionSociedad_form.value.nombre,
              fecha_pago: this.renovacionSociedad_form.value.fecha_pago,
              periocidad: this.renovacionSociedad_form.value.periocidad,
              valor: this.renovacionSociedad_form.value.valor,
              tipo_moneda: this.renovacionSociedad_form.value.tipo_moneda
            }
            this.dialogRef.close(objeto);
          }
          break;
      case 'obligacionesSociedad':
        if(this.obligacionSociedad_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objeto = {
            nombre: this.obligacionSociedad_form.value.nombre,
            fecha_pago: this.obligacionSociedad_form.value.fecha_pago,
            periocidad: this.obligacionSociedad_form.value.periocidad,
            valor: this.obligacionSociedad_form.value.valor,
            tipo_moneda: this.obligacionSociedad_form.value.tipo_moneda
          }
          this.dialogRef.close(objeto);
        }
        break;
      case 'pagosPrimas':
        if(this.pagosPrimas_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objeto = {
            nombre: this.pagosPrimas_form.value.nombre,
            fecha_pago: this.pagosPrimas_form.value.fecha_pago,
            periocidad: this.pagosPrimas_form.value.periocidad,
            valor: this.pagosPrimas_form.value.valor,
            tipo_moneda: this.pagosPrimas_form.value.tipo_moneda
          }
          this.dialogRef.close(objeto);
        }
        break;
      case 'ingresosSalario':
        if(this.ingresoRecurrente_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objeto = {
            nombre: this.ingresoRecurrente_form.value.nombre,
            fecha_pago: this.ingresoRecurrente_form.value.fecha_pago,
            periocidad: this.ingresoRecurrente_form.value.periocidad,
            valor: this.ingresoRecurrente_form.value.valor,
            tipo_moneda: this.ingresoRecurrente_form.value.tipo_moneda
          }
          this.dialogRef.close(objeto);
        }
        break;
      case 'egresosRecurrente':
        if(this.egresoRecurrente_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objeto = {
            nombre: this.egresoRecurrente_form.value.nombre,
            fecha_pago: this.egresoRecurrente_form.value.fecha_pago,
            periocidad: this.egresoRecurrente_form.value.periocidad,
            valor: this.egresoRecurrente_form.value.valor,
            tipo_moneda: this.egresoRecurrente_form.value.tipo_moneda
          }
          this.dialogRef.close(objeto);
        }
        break;
      case 'contactos':
        if(this.contactos_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoCon = {
            nombre: this.contactos_form.value.nombre,
            empresa: this.contactos_form.value.empresa,
            telefono: this.contactos_form.value.telefono,
            celular: this.contactos_form.value.celular,
            correo: this.contactos_form.value.correo
          }
          this.dialogRef.close(objetoCon);
        }
        break;
      case 'files':
        if(this.files_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoFile = {
            nombre: this.files_form.value.nombre,
            file: this.archivoSubir,
            categoria: this.files_form.value.categoria,
            subcategoria: this.files_form.value.subcategoria,
            documento: this.files_form.value.documento,
            notas: this.files_form.value.notas
          }
          console.log('objeto file');
          console.log(objetoFile);
          this.dialogRef.close(objetoFile);
        }
        break;
      case 'eventos':
        if(this.eventos_form.invalid) {
          this.showErrorPopUp();
        } else {
          let objetoEvento = {
            nota: this.eventos_form.value.nota,
            fecha: this.eventos_form.value.fecha,
          }
          this.dialogRef.close(objetoEvento);
        }
        break;
        case 'todos':
          if(this.todos_form.invalid) {
            this.showErrorPopUp();
          } else {
            let objeto = {
              nombre: this.todos_form.value.nombre,
              fecha: this.todos_form.value.fecha,
              state: this.todos_form.value.state
            }
            this.dialogRef.close(objeto);
          }
          break;
    }

  }

}
