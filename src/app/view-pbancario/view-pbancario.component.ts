import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { ProductoBancario } from 'app/interfaces/pro_bancario.interface';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ActivoService } from 'app/services/activo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-pbancario',
  templateUrl: './view-pbancario.component.html',
  styleUrls: ['./view-pbancario.component.scss']
})
export class ViewPbancarioComponent implements OnInit {

  @ViewChild(MatPaginator) paginatorContactos: MatPaginator;
  @ViewChild(MatSort) sortContactos: MatSort;
  @ViewChild(MatPaginator) paginatorDocumentos: MatPaginator;
  @ViewChild(MatSort) sortDocumentos: MatSort;

  loader: boolean;
  pbancario: ProductoBancario;
  contactos: MatTableDataSource<any>;
  documentos: MatTableDataSource<any>;
  ultimosContactos: any[] = [];
  displayedColumnsContactos: string[] = ['Nombre', 'Empresa', 'Telefono', 'Celular', 'Correo'];
  displayedColumnsDocumentos: string[] = ['Nombre del documento', 'Acciones'];
  edicion: boolean;
  documento: File;
  nombreDocumento: string;
  periocidadTypes: any[];
  colorBoton = '#bdbdbd';

  pago_cuotas = {
    "fecha_pago": '',
    "periocidad": '',
  };

  constructor(private activateRoute: ActivatedRoute, private activoService: ActivoService, public dialog: MatDialog) { 
    this.loader = false;
    this.edicion = false;
  }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((params: any) => {
      this.loader = true;
      this.infoPBancario(params._id);
    });
    this.asignacionSelects();
  }

  asignacionSelects() {
    this.periocidadTypes = [
      {value: 'Diario', viewValue: 'Diario'},
      {value: 'Semanal', viewValue: 'Semanal'},
      {value: 'Mensual', viewValue: 'Mensual'},
      {value: 'Semestral', viewValue: 'Semestral'},
      {value: 'Anual', viewValue: 'Anual  '},
    ]
  }

  infoPBancario(id: any) {
    this.activoService.getPbancario(id)
      .subscribe((producto: ProductoBancario) => {
        this.pbancario = producto;
        this.contactos = new MatTableDataSource<any>(producto.contactos);
        this.ultimosContactos = this.pbancario.contactos;
        this.documentos = new MatTableDataSource<any>(producto.documentos);
        this.pago_cuotas = this.pbancario.pago_cuotas ? this.pbancario.pago_cuotas : this.pago_cuotas;
        this.loader = false;
      })
  }

  ngAfterViewInit() {
    this.contactos.paginator = this.paginatorContactos;
    this.contactos.sort = this.sortContactos;
    this.documentos.paginator = this.paginatorDocumentos;
    this.documentos.sort = this.sortDocumentos;
  }

  applyFilterContactos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contactos.filter = filterValue.trim().toLowerCase();

    if (this.contactos.paginator) {
      this.contactos.paginator.firstPage();
    }
  }

  applyFilterDocumentos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  editFicha() {
    this.edicion = true;
    this.colorBoton = '#ef8354';
  }

  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'contactos':
            this.ultimosContactos.push(result);
            this.contactos = new MatTableDataSource<any>(this.ultimosContactos);
            break;
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            Swal.fire({
              title: 'Cargue de documentos',
              text: "Al cargar el documento se recargará la ventana y perderás la información editada en los anteriores ítems. Se recomienda primero guardar los datos anteriores. ¿Deseas continuar?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ef8354',
              cancelButtonColor: '#bdbdbd',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si'
            }).then((result) => {
              if (result.isConfirmed) {
                this.loader = true;
                this.activoService.uploadFileProductoB(this.pbancario._id, this.documento, this.nombreDocumento)
                .then(res => {
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Documento cargado exitosamente',
                    showConfirmButton: false,
                    timer: 2500
                  });
                  window.location.reload();
                })
                .catch((error) => {
                  Swal.fire('Error', error.error.error.message, 'error');
                });
              } else if (result.isDenied) {
                return;
              }
            })
            break;
        }
      } 
    });
  }

  guardar() {
    this.pbancario.pago_cuotas = this.pago_cuotas;
    this.pbancario.contactos = this.ultimosContactos;
    this.activoService.updatePbancario(this.pbancario._id, this.pbancario)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Producto Bancario Actualizado Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
      }
    })
  }

}
