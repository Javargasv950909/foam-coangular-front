import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPbancarioComponent } from './view-pbancario.component';

describe('ViewPbancarioComponent', () => {
  let component: ViewPbancarioComponent;
  let fixture: ComponentFixture<ViewPbancarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewPbancarioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewPbancarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
