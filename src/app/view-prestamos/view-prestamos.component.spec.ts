import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPrestamosComponent } from './view-prestamos.component';

describe('ViewPrestamosComponent', () => {
  let component: ViewPrestamosComponent;
  let fixture: ComponentFixture<ViewPrestamosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewPrestamosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewPrestamosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
