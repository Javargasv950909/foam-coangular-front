import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/services/login.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: '/what-have', title: '¿Qué Tengo?',  icon:'inventory', class: '' },
    { path: '/calendar-user', title: 'Mis eventos',  icon:'event', class: '' },
    { path: '/documents-user', title: 'Mis Documentos',  icon:'folder', class: '' },
    { path: '/user-profile', title: 'Mi Perfil',  icon:'person', class: '' },
    { path: '/faqs', title: 'FAQ',  icon:'quiz', class: '' },
    { path: '/support', title: 'Soporte',  icon:'support_agent', class: '' },
    // { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    // { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
    // { path: '/upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };

  logOut() {
    this.loginService.logOut();
}
}
