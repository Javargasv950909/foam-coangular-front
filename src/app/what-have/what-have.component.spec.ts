import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatHaveComponent } from './what-have.component';

describe('WhatHaveComponent', () => {
  let component: WhatHaveComponent;
  let fixture: ComponentFixture<WhatHaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WhatHaveComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WhatHaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
