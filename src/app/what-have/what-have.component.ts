import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { User } from 'app/interfaces/user.interface';
import { UserService } from 'app/services/user.service';
import Swal from 'sweetalert2'
import { Inmueble } from 'app/interfaces/inmueble.interface';
import { UtilService } from 'app/services/util.service';
import { Vehiculo } from 'app/interfaces/vehiculo.interface';
import { ActivoService } from 'app/services/activo.service';
import { Sociedad } from 'app/interfaces/sociedades.interface';
import { PolizaSeguro } from 'app/interfaces/poliza_seguro.interface';
import { Inversion } from 'app/interfaces/inversion.interface';
import { ProductoBancario } from 'app/interfaces/pro_bancario.interface';
import { Prestamo } from 'app/interfaces/prestamos.interface';
import { Salario } from 'app/interfaces/salario.interface';
import { Membresia } from 'app/interfaces/membresia.interface';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalCalendarComponent } from 'app/modal-calendar/modal-calendar.component';

@Component({
  selector: 'app-what-have',
  templateUrl: './what-have.component.html',
  styleUrls: ['./what-have.component.scss']
})
export class WhatHaveComponent implements OnInit {

  @ViewChild(MatPaginator) paginatorInmueble: MatPaginator;
  @ViewChild(MatSort) sortInmueble: MatSort;
  @ViewChild(MatPaginator) paginatorVehiculo: MatPaginator;
  @ViewChild(MatSort) sortVehiculo: MatSort;
  @ViewChild(MatPaginator) paginatorSociedad: MatPaginator;
  @ViewChild(MatSort) sortSociedad: MatSort;
  @ViewChild(MatPaginator) paginatorPoliza: MatPaginator;
  @ViewChild(MatSort) sortPoliza: MatSort;
  @ViewChild(MatPaginator) paginatorInversion: MatPaginator;
  @ViewChild(MatSort) sortInversion: MatSort;
  @ViewChild(MatPaginator) paginatorPbancario: MatPaginator;
  @ViewChild(MatSort) sortPbancario: MatSort;
  @ViewChild(MatPaginator) paginatorPrestamo: MatPaginator;
  @ViewChild(MatSort) sortPrestamo: MatSort;
  @ViewChild(MatPaginator) paginatorSalario: MatPaginator;
  @ViewChild(MatSort) sortSalario: MatSort;
  @ViewChild(MatPaginator) paginatorMembresia: MatPaginator;
  @ViewChild(MatSort) sortMembresia: MatSort;

  loader: boolean;
  user: User;
  inmuebles: MatTableDataSource<Inmueble>;
  vehiculos: MatTableDataSource<Vehiculo>;
  sociedades: MatTableDataSource<Sociedad>;
  polizas: MatTableDataSource<PolizaSeguro>;
  inversiones: MatTableDataSource<Inversion>;
  productos_bancarios: MatTableDataSource<ProductoBancario>;
  prestamos: MatTableDataSource<Prestamo>;
  salarios: MatTableDataSource<Salario>;
  membresias: MatTableDataSource<Membresia>;
  displayedColumns: string[] = ['subcategoria', 'nombre', 'matricula inmobiliaria', 'ubicacion', 'acciones'];
  displayedColumnsVehiculos: string[] = ['subcategoria', 'nombre', 'tarjeta de propiedad', 'ubicacion', 'acciones'];
  displayedColumnsSociedad: string[] = ['subcategoria', 'razon social', 'nit', 'ubicacion', 'acciones'];
  displayedColumnsPoliza: string[] = ['subcategoria', 'nombre', 'activo', 'compañia', 'acciones'];
  displayedColumnsInversion: string[] = ['subcategoria', 'nombre', 'entidad', 'valor', 'acciones'];
  displayedColumnsPbancario: string[] = ['subcategoria', 'nombre', 'entidad', 'numproducto', 'acciones'];
  displayedColumnsPrestamo: string[] = ['subcategoria', 'nombre', 'entidad o persona', 'valor', 'acciones'];
  displayedColumnsSalario: string[] = ['subcategoria', 'nombre', 'entidad o persona', 'valor', 'acciones'];
  displayedColumnsMembresia: string[] = ['subcategoria', 'nombre', 'ubicacion', 'entidad', 'acciones'];

  constructor(private userService: UserService, private router: Router, private utilService: UtilService, private activoService: ActivoService, public dialog: MatDialog) {
    this.listarActivos();
    this.loader = false;
  }
  
  ngOnInit(): void {
  }
  
  listarActivos() {
    this.loader = true;
    this.userService.getUser().subscribe({
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next:(resp: User) => {
        this.user = resp;
        // Se asigna valores al render de las tablas dinamicas
        this.inmuebles = new MatTableDataSource<Inmueble>(this.user.inmuebles);
        this.vehiculos = new MatTableDataSource<Vehiculo>(this.user.vehiculos);
        this.sociedades = new MatTableDataSource<Sociedad>(this.user.sociedades);
        this.polizas = new MatTableDataSource<PolizaSeguro>(this.user.polizas_seguro);
        this.inversiones = new MatTableDataSource<Inversion>(this.user.inversiones);
        this.productos_bancarios = new MatTableDataSource<ProductoBancario>(this.user.productos_bancarios);
        this.prestamos = new MatTableDataSource<Prestamo>(this.user.prestamos);
        this.salarios = new MatTableDataSource<Salario>(this.user.salarios);
        this.membresias = new MatTableDataSource<Membresia>(this.user.membresias);
        this.loader = false;
      }
    });
  }
  
  ngAfterViewInit() {
    this.inmuebles.paginator = this.paginatorInmueble;
    this.inmuebles.sort = this.sortInmueble;
    this.vehiculos.paginator = this.paginatorVehiculo;
    this.vehiculos.sort = this.sortVehiculo;
    this.sociedades.paginator = this.paginatorSociedad;
    this.sociedades.sort = this.sortSociedad;
    this.polizas.paginator = this.paginatorPoliza;
    this.polizas.sort = this.sortPoliza;
    this.inversiones.paginator = this.paginatorInversion;
    this.inversiones.sort = this.sortInversion;
    this.productos_bancarios.paginator = this.paginatorPbancario;
    this.productos_bancarios.sort = this.sortPbancario;
    this.prestamos.paginator = this.paginatorPrestamo;
    this.prestamos.sort = this.sortPrestamo;
    this.salarios.paginator = this.paginatorSalario;
    this.salarios.sort = this.sortSalario;
    this.membresias.paginator = this.paginatorMembresia;
    this.membresias.sort = this.sortMembresia;
  }

  applyFilterInmueble(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.inmuebles.filter = filterValue.trim().toLowerCase();

    if (this.inmuebles.paginator) {
      this.inmuebles.paginator.firstPage();
    }
  }

  applyFilterVehiculo(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.vehiculos.filter = filterValue.trim().toLowerCase();

    if (this.vehiculos.paginator) {
      this.vehiculos.paginator.firstPage();
    }
  }

  applyFilterSociedad(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.sociedades.filter = filterValue.trim().toLowerCase();

    if (this.sociedades.paginator) {
      this.sociedades.paginator.firstPage();
    }
  }

  applyFilterPoliza(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.polizas.filter = filterValue.trim().toLowerCase();

    if (this.polizas.paginator) {
      this.polizas.paginator.firstPage();
    }
  }

  applyFilterInversion(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.inversiones.filter = filterValue.trim().toLowerCase();

    if (this.inversiones.paginator) {
      this.inversiones.paginator.firstPage();
    }
  }

  applyFilterPbancario(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.productos_bancarios.filter = filterValue.trim().toLowerCase();

    if (this.productos_bancarios.paginator) {
      this.productos_bancarios.paginator.firstPage();
    }
  }

  applyFilterPrestamo(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.prestamos.filter = filterValue.trim().toLowerCase();

    if (this.prestamos.paginator) {
      this.prestamos.paginator.firstPage();
    }
  }

  applyFilterSalario(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.salarios.filter = filterValue.trim().toLowerCase();

    if (this.salarios.paginator) {
      this.salarios.paginator.firstPage();
    }
  }

  applyFilterMembresia(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.membresias.filter = filterValue.trim().toLowerCase();

    if (this.membresias.paginator) {
      this.membresias.paginator.firstPage();
    }
  }

  nuevo() {
    this.router.navigateByUrl('/new-active')
  }
  
  openPutActivo(activo: any) {
    switch(activo.id_subtg) {
      case '1':
        this.utilService.goToFichInmueble(activo);
        break;
      case '2':
        this.utilService.goToFichVehiculo(activo);
        break;
      case '3':
        this.utilService.goToFichSociedad(activo);
        break;
      case '4':
        this.utilService.goToFichPoliza(activo);
        break;
      case '5':
        this.utilService.goToFichInversion(activo);
        break;
      case '6':
        this.utilService.goToFichPbancario(activo);
        break;
      case '7':
        this.utilService.goToFichPrestamo(activo);
        break;
      case '8':
        this.utilService.goToFichSalario(activo);
        break;
      case '9':
        this.utilService.goToFichMembresia(activo);
        break;
    }
  }

  openRemember(activo: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'custom-dialog'; 
    const dialogRef = this.dialog.open(ModalCalendarComponent, {
      data: activo
    });
  }

  deleteActivo(activo: any) {
    Swal.fire({
      title: '¿Estás seguro que deseas eliminar el recuerdame?',
      text: 'Recuerda que esta acción es permanente.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#ef8354',
      cancelButtonColor: '#bdbdbd',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí, Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.activoService.deleteActivo(activo._id, activo.id_subtg)
          .subscribe(any => {
            Swal.fire(
              'Eliminado!',
              `${activo.subcategoria} ${activo.nombre} eliminado exitosamente.`,
              'success'
            )
            window.location.reload();
          })
      }
    })
  }
}
