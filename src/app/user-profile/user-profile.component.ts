import { Component, OnInit } from '@angular/core';
import { User } from 'app/interfaces/user.interface';
import { UserService } from 'app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user: User;
  loader: boolean;
  documentsTypes: any[];
  edicion: boolean;

  constructor(private userService: UserService) {
    this.loader = false;
    this.edicion = false;
  }

  ngOnInit() {
    this.getUserInfo();
    this.documentsTypes = [
      {value: 'Cedula de ciudadanía', viewValue: 'Cédula de ciudadania'},
      {value: 'Cedula de extranjería', viewValue: 'Cédula de extranjeria'},
    ]
  }

  getUserInfo() {
    this.userService.getUser().subscribe({
      next:(resp: User) => {
        this.user = resp;
        this.loader = false;
      }
    });
  }

  actualizarUser() {
    this.edicion = true;
  }

  guardar() {
    Swal.fire({
      title: 'Actualización de usuario',
      text: "¿Deseas confirmar la actualización?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#ef8354',
      cancelButtonColor: '#bdbdbd',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar cambios'
    }).then((result) => {
      if (result.isConfirmed) {
        this.loader = true;
        this.userService.updateUser(this.user._id, this.user)
        .subscribe( {
          error: (e:any) => {
            Swal.fire('Error', e.error.error.message, 'error');
          },
          next: (res) => {
            Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Usuario Actualizado Exitosamente',
              showConfirmButton: false,
              timer: 2500
            });
          }
        })
      } else if (result.isDenied) {
        return;
      }
    })
  }

  calcularEdad() {
    console.log('entre al change')
    if (this.user.born_date) {
      const hoy = new Date();
      const fechaNacimiento = new Date(this.user.born_date);
      const diff = hoy.getTime() - fechaNacimiento.getTime();
      this.user.age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
    } else {
      this.user.age = null;
    }
  }


}
