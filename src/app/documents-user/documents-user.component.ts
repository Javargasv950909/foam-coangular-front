import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'app/interfaces/user.interface';
import { ModalCalendarComponent } from 'app/modal-calendar/modal-calendar.component';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { UserService } from 'app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-documents-user',
  templateUrl: './documents-user.component.html',
  styleUrls: ['./documents-user.component.scss']
})
export class DocumentsUserComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  user: User;
  loader: boolean;
  documentos: MatTableDataSource<any>;
  displayedColumnsDocumentos: string[] = ['Categoria', 'Subcategoria', 'Nombre del documento', 'Documento', 'Notas', 'Acciones'];
  documento: File;
  nombreDocumento: string;
  categoria: string;
  subcategoria: string;
  num_documento: string;
  notas: string;

  constructor(public dialog: MatDialog, private userService: UserService) { 
    this.loader = false;
  }

  ngOnInit(): void {
    this.loader = true;
    this.listarDocumentos();
  }

  listarDocumentos() {
    this.userService.getUser().subscribe({
      next:(resp: User) => {
        this.user = resp;
        this.documentos = new MatTableDataSource<any>(this.user.documents);
        this.loader = false;
      }
    });
  }

  ngAfterViewInit() {
    this.documentos.paginator = this.paginator;
    this.documentos.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  openRemember(document: any) {
    this.dialog.open(ModalCalendarComponent, {
      data: document
    });
  }

  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            this.categoria = result.categoria;
            this.subcategoria = result.subcategoria;
            this.num_documento = result.documento ? result.documento : '';
            this.notas = result.notas ? result.notas : '';
            this.loader = true;
            this.userService.uploadFileUser(this.user._id, this.documento, this.nombreDocumento, '10', this.categoria, this.subcategoria, this.num_documento, this.notas)
            .then(res => {
              Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Documento cargado exitosamente',
                showConfirmButton: false,
                timer: 2500
              });
              this.loader = false;
              window.location.reload();
            })
            break;
        }
      } 
    });
  }

  openEdit(document: any) {
    
  }

  deleteDocument(document: any) {
    Swal.fire({
      title: '¿Estás seguro que deseas eliminar el documento?',
      text: 'Recuerda que esta acción es permanente.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#ef8354',
      cancelButtonColor: '#bdbdbd',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí, Eliminar'
    }).then(async (result) => {
      if (result.isConfirmed) {
        (await this.userService.deleteFileUser(document._id))
          .subscribe(any => {
            Swal.fire(
              'Eliminado!',
              `${document.name} eliminado exitosamente.`,
              'success'
            )
            window.location.reload();
          })
      }
    })
  }

}
