import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Evento } from 'app/interfaces/evento.interface';
import { CalendarOptions, EventClickArg, EventApi } from '@fullcalendar/core'; // useful for typechecking
import esLocale from '@fullcalendar/core/locales/es';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import Swal from 'sweetalert2';
import { UserService } from 'app/services/user.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalCalendarComponent } from 'app/modal-calendar/modal-calendar.component';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  user: any;
  loader: boolean;
  eventos: any[] = []
  currentEvents: EventApi[] = [];
  calendarOptions: CalendarOptions;

  constructor(private changeDetector: ChangeDetectorRef, private userService: UserService, public dialog: MatDialog) {
    this.loader = false;
   }

  ngOnInit(): void {
    this.getEvents();
  }

  async getEvents() {
    this.loader = true;
  
    try {
      const user = await this.userService.getUser().toPromise();
      this.user = user;
  
      this.user.reminders.forEach((reminder: Evento) => {
        let currentDate = new Date(reminder.fecha);
        let endDate = new Date(); 
        endDate.setFullYear(endDate.getFullYear() + 1);
  
        // Agregar el evento solo si la periodicidad no es 'no se repite'
        if (reminder.periocidad.toLowerCase() !== 'no se repite') {
          while (currentDate <= endDate) {
            let obj = {
              title: reminder.nombre,
              date: new Date(currentDate),
              periocidad: reminder.periocidad,
              valor: reminder.valor
            };
  
            this.eventos.push(obj);
  
            // Incrementa la fecha según la periodicidad
            switch (reminder.periocidad.toLowerCase()) {
              case 'diario':
                currentDate.setDate(currentDate.getDate() + 1);
                break;
              case 'semanal':
                currentDate.setDate(currentDate.getDate() + 7);
                break;
              case 'mensual':
                currentDate.setMonth(currentDate.getMonth() + 1);
                break;
              case 'semestral':
                currentDate.setMonth(currentDate.getMonth() + 6);
                break;
              case 'anual':
                currentDate.setFullYear(currentDate.getFullYear() + 1);
                break;
            }
          }
        } else {
          // Agregar el evento único en caso de 'no se repite'
          let obj = {
            title: reminder.nombre,
            date: new Date(currentDate),
            periocidad: reminder.periocidad,
            valor: reminder.valor
          };
          this.eventos.push(obj);
        }
      });
  
      this.initCalendar();
    } catch (error) {
      console.error('Error al obtener los eventos:', error);
    } finally {
      this.loader = false;
    }
  }  

  initCalendar() {
    this.calendarOptions = {
      plugins: [
        interactionPlugin,
        dayGridPlugin,
        timeGridPlugin,
        listPlugin,
      ],
      eventContent: this.customEventContent,
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      initialView: 'dayGridMonth',
      initialEvents: this.eventos,
      weekends: true,
      editable: true,
      selectable: true,
      selectMirror: true,
      dayMaxEvents: true,
      locale: esLocale,
      eventClick: this.handleEventClick.bind(this),
      eventsSet: this.handleEvents.bind(this)
    };
  }

  handleDateClick(arg) {
    alert('date click! ' + arg.dateStr)
  }

  customEventContent = (arg: { event: EventApi }) => {
    return { html: `<div class='evento'>${arg.event.title}</div>` }; // Mostrar solo el nombre del evento
  }

  handleEventClick(clickInfo: EventClickArg) {
    console.log(clickInfo.event.extendedProps.valor)
    if(!clickInfo.event.extendedProps.valor) {
      Swal.fire({
        icon: 'info',
        title: `${clickInfo.event.title}`,
        text: `Periocidad de la notificación: ${clickInfo.event.extendedProps.periocidad}`,
      })
    } else {
      Swal.fire({
        icon: 'info',
        title: `${clickInfo.event.title}`,
        text: `Periocidad de la notificación: ${clickInfo.event.extendedProps.periocidad}, valor: ${clickInfo.event.extendedProps.valor}`,
      })
    }
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
    this.changeDetector.detectChanges();
  }

  openRemember() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'custom-dialog'; 
    const dialogRef = this.dialog.open(ModalCalendarComponent, {
      data: 'new'
    });
    dialogRef.afterClosed().subscribe(result => {
      window.location.reload();
    });
  }

}
