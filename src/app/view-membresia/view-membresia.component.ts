import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Membresia } from 'app/interfaces/membresia.interface';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ActivoService } from 'app/services/activo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-membresia',
  templateUrl: './view-membresia.component.html',
  styleUrls: ['./view-membresia.component.scss']
})
export class ViewMembresiaComponent implements OnInit {

  @ViewChild(MatPaginator) paginatorEgreso: MatPaginator;
  @ViewChild(MatSort) sortEgreso: MatSort;
  @ViewChild(MatPaginator) paginatorContactos: MatPaginator;
  @ViewChild(MatSort) sortContactos: MatSort;
  @ViewChild(MatPaginator) paginatorDocumentos: MatPaginator;
  @ViewChild(MatSort) sortDocumentos: MatSort;

  loader: boolean;
  membresia: Membresia;
  egresos: MatTableDataSource<any>;
  contactos: MatTableDataSource<any>;
  documentos: MatTableDataSource<any>;
  ultimosEgresos: any[] = [];
  ultimosContactos: any[] = [];
  displayedColumnEgresos: string[] = ['Nombre', 'Fecha de pago', 'Periocidad', 'valor', 'Moneda'];
  displayedColumnsContactos: string[] = ['Nombre', 'Empresa', 'Telefono', 'Celular', 'Correo'];
  displayedColumnsDocumentos: string[] = ['Nombre del documento', 'Acciones'];
  edicion: boolean;
  documento: File;
  nombreDocumento: string;
  colorBoton = '#bdbdbd';

  constructor(private activateRoute: ActivatedRoute, private activoService: ActivoService, public dialog: MatDialog) { 
    this.loader = false;
    this.edicion = false;
  }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((params: any) => {
      this.loader = true;
      this.infoMembresia(params._id);
    });
  }

  infoMembresia(id: any) {
    this.activoService.getMembresia(id)
      .subscribe((membresia: Membresia) => {
        this.membresia = membresia;
        this.egresos = new MatTableDataSource<any>(membresia.egreso_recurrente);
        this.ultimosEgresos = this.membresia.egreso_recurrente;
        this.contactos = new MatTableDataSource<any>(membresia.contactos);
        this.ultimosContactos = this.membresia.contactos;
        this.documentos = new MatTableDataSource<any>(membresia.documentos);
        this.loader = false;
      })
  }

  ngAfterViewInit() {
    this.egresos.paginator = this.paginatorEgreso;
    this.egresos.sort = this.sortEgreso;
    this.contactos.paginator = this.paginatorContactos;
    this.contactos.sort = this.sortContactos;
    this.documentos.paginator = this.paginatorDocumentos;
    this.documentos.sort = this.sortDocumentos;
  }

  applyFilterEgresos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.egresos.filter = filterValue.trim().toLowerCase();

    if (this.egresos.paginator) {
      this.egresos.paginator.firstPage();
    }
  }

  applyFilterContactos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contactos.filter = filterValue.trim().toLowerCase();

    if (this.contactos.paginator) {
      this.contactos.paginator.firstPage();
    }
  }

  applyFilterDocumentos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  editFicha() {
    this.edicion = true;
    this.colorBoton = '#ef8354';
  }
  
  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'egresosRecurrente':
            this.ultimosEgresos.push(result);
            this.egresos = new MatTableDataSource<any>(this.ultimosEgresos);
            break;
          case 'contactos':
            this.ultimosContactos.push(result);
            this.contactos = new MatTableDataSource<any>(this.ultimosContactos);
            break;
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            Swal.fire({
              title: 'Cargue de documentos',
              text: "Al cargar el documento se recargará la ventana y perderás la información editada en los anteriores ítems. Se recomienda primero guardar los datos anteriores. ¿Deseas continuar?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ef8354',
              cancelButtonColor: '#bdbdbd',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si'
            }).then((result) => {
              if (result.isConfirmed) {
                this.loader = true;
                this.activoService.uploadFileMembresia(this.membresia._id, this.documento, this.nombreDocumento)
                .then(res => {
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Documento cargado exitosamente',
                    showConfirmButton: false,
                    timer: 2500
                  });
                  window.location.reload();
                })
                .catch((error) => {
                  Swal.fire('Error', error.error.error.message, 'error');
                });
              } else if (result.isDenied) {
                return;
              }
            })
            break;
        }
      } 
    });
  }

  guardar() {
    this.membresia.egreso_recurrente = this.ultimosEgresos;
    this.membresia.contactos = this.ultimosContactos;
    this.activoService.updateMembresia(this.membresia._id, this.membresia)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Membresia Actualizada Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
      }
    })
  }

}
