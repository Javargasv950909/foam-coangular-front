import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInversionComponent } from './view-inversion.component';

describe('ViewInversionComponent', () => {
  let component: ViewInversionComponent;
  let fixture: ComponentFixture<ViewInversionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewInversionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
