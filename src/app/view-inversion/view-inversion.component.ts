import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Inversion } from 'app/interfaces/inversion.interface';
import { ModalCustomComponent } from 'app/modal-custom/modal-custom.component';
import { ActivoService } from 'app/services/activo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-inversion',
  templateUrl: './view-inversion.component.html',
  styleUrls: ['./view-inversion.component.scss']
})
export class ViewInversionComponent implements OnInit {

  @ViewChild(MatPaginator) paginatorIntereses: MatPaginator;
  @ViewChild(MatSort) sortIntereses: MatSort;
  @ViewChild(MatPaginator) paginatorContactos: MatPaginator;
  @ViewChild(MatSort) sortContactos: MatSort;
  @ViewChild(MatPaginator) paginatorDocumentos: MatPaginator;
  @ViewChild(MatSort) sortDocumentos: MatSort;
  @ViewChild(MatPaginator) paginatorEventos: MatPaginator;
  @ViewChild(MatSort) sortEventos: MatSort;
  @ViewChild(MatPaginator) paginatorTodos: MatPaginator;
  @ViewChild(MatSort) sortTodos: MatSort;

  loader: boolean;
  inversion: Inversion;
  intereses: MatTableDataSource<any>;
  contactos: MatTableDataSource<any>;
  documentos: MatTableDataSource<any>;
  ultimosIntereses: any[] = [];
  ultimosContactos: any[] = [];
  ultimosEventos: any[] = [];
  ultimosTodos: any[] = [];
  eventos: MatTableDataSource<any>;
  todos: MatTableDataSource<any>;
  displayedColumnIntereses: string[] = ['Nombre', 'Fecha de pago', 'Periocidad', 'valor', 'Moneda'];
  displayedColumnsContactos: string[] = ['Nombre', 'Empresa', 'Telefono', 'Celular', 'Correo'];
  displayedColumnsDocumentos: string[] = ['Nombre del documento', 'Acciones'];
  displayedColumnsEventos: string[] = ['Fecha', 'Nota'];
  displayedColumnsTodos: string[] = ['State', 'Nombre', 'Fecha'];
  edicion: boolean;
  documento: File;
  nombreDocumento: string;
  colorBoton = '#bdbdbd';

  constructor(private activateRoute: ActivatedRoute, private activoService: ActivoService, public dialog: MatDialog) { 
    this.loader = false;
    this.edicion = false;
  }

  ngOnInit(): void {
    this.activateRoute.queryParams
    .subscribe((params: any) => {
      this.loader = true;
      this.infoInversion(params._id);
    });
  }

  infoInversion(id: any) {
    this.activoService.getInversion(id)
      .subscribe((inversion: Inversion) => {
        this.inversion = inversion;
        this.intereses = new MatTableDataSource<any>(inversion.intereses_dividendos);
        this.ultimosIntereses = this.inversion.intereses_dividendos;
        this.contactos = new MatTableDataSource<any>(inversion.contactos);
        this.ultimosContactos = this.inversion.contactos;
        this.documentos = new MatTableDataSource<any>(inversion.documentos);
        this.eventos = new MatTableDataSource<any>(inversion.eventos);
        this.ultimosEventos = this.inversion.eventos;
        this.todos = new MatTableDataSource<any>(inversion.todos);
        this.todos.filterPredicate = (data: any) => !data.state;
        this.todos.filter = 'excludeFalse';
        this.ultimosTodos = this.inversion.todos;
        this.loader = false;
      })
  }

  ngAfterViewInit() {
    this.intereses.paginator = this.paginatorIntereses;
    this.intereses.sort = this.sortIntereses;
    this.contactos.paginator = this.paginatorContactos;
    this.contactos.sort = this.sortContactos;
    this.documentos.paginator = this.paginatorDocumentos;
    this.documentos.sort = this.sortDocumentos;
    this.eventos.paginator = this.paginatorEventos;
    this.eventos.sort = this.sortEventos;
    this.todos.paginator = this.paginatorTodos;
    this.todos.sort = this.sortTodos;
  }

  applyFilterIntereses(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.intereses.filter = filterValue.trim().toLowerCase();

    if (this.intereses.paginator) {
      this.intereses.paginator.firstPage();
    }
  }

  applyFilterContactos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contactos.filter = filterValue.trim().toLowerCase();

    if (this.contactos.paginator) {
      this.contactos.paginator.firstPage();
    }
  }

  applyFilterDocumentos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.documentos.filter = filterValue.trim().toLowerCase();

    if (this.documentos.paginator) {
      this.documentos.paginator.firstPage();
    }
  }

  applyFilterEventos(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.eventos.filter = filterValue.trim().toLowerCase();

    if (this.eventos.paginator) {
      this.eventos.paginator.firstPage();
    }
  }

  editFicha() {
    this.edicion = true;
    this.colorBoton = '#ef8354';
  }

  openInsert(path: any) {
    const dialogRef = this.dialog.open(ModalCustomComponent, {
      data: path
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        switch(path) {
          case 'interesesDividendos':
            this.ultimosIntereses.push(result);
            this.intereses = new MatTableDataSource<any>(this.ultimosIntereses);
            break;
          case 'contactos':
            this.ultimosContactos.push(result);
            this.contactos = new MatTableDataSource<any>(this.ultimosContactos);
            break;
          case 'eventos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosEventos);
            this.ultimosEventos.push(result);
            this.eventos = new MatTableDataSource<any>(this.ultimosEventos);
            break;
          case 'todos':
            console.log('este es el result');
            console.log(result)
            console.log(this.ultimosTodos);
            this.ultimosTodos.push(result);
            this.todos = new MatTableDataSource<any>(this.ultimosTodos);
            break;
          case 'files':
            this.documento = result.file;
            this.nombreDocumento = result.nombre;
            Swal.fire({
              title: 'Cargue de documentos',
              text: "Al cargar el documento se recargará la ventana y perderás la información editada en los anteriores ítems. Se recomienda primero guardar los datos anteriores. ¿Deseas continuar?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ef8354',
              cancelButtonColor: '#bdbdbd',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si'
            }).then((result) => {
              if (result.isConfirmed) {
                this.loader = true;
                this.activoService.uploadFileInversion(this.inversion._id, this.documento, this.nombreDocumento)
                .then(res => {
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Documento cargado exitosamente',
                    showConfirmButton: false,
                    timer: 2500
                  });
                  window.location.reload();
                })
                .catch((error) => {
                  Swal.fire('Error', error.error.error.message, 'error');
                });
              } else if (result.isDenied) {
                return;
              }
            })
            break;
        }
      } 
    });
  }

  guardar() {
    this.inversion.intereses_dividendos = this.ultimosIntereses;
    this.inversion.contactos = this.ultimosContactos;
    this.inversion.eventos = this.ultimosEventos;
    this.activoService.updateInversion(this.inversion._id, this.inversion)
    .subscribe( {
      error: (e:any) => {
        Swal.fire('Error', e.error.error.message, 'error');
      },
      next: (res) => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Inversión Actualizada Exitosamente',
          showConfirmButton: false,
          timer: 2500
        });
      }
    })
  }

}
