import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cashformat'
})
export class CashformatPipe implements PipeTransform {

  transform(value: number | string): string {
    // Si el valor no es numérico, devolverlo sin cambios
    if (isNaN(Number(value))) {
      return value as string;
    }

    // Formatear como cadena de moneda
    const formatter = new Intl.NumberFormat('es-ES', {
      style: 'currency',
      currency: 'EUR' // Puedes cambiar la moneda según tus necesidades
    });

    return formatter.format(Number(value));
  }

}
